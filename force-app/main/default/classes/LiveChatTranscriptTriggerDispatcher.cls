public class LiveChatTranscriptTriggerDispatcher implements TriggerDispatcher{
    private LiveChatTranscriptHelper helper;
    public LiveChatTranscriptTriggerDispatcher() { 
        helper = new LiveChatTranscriptHelper(); 
    }
    public void bulkBefore() {
        System.debug('LiveChatTranscript (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('LiveChatTranscript (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('LiveChatTranscript (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('LiveChatTranscript (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('LiveChatTranscript (Before Update)');
        helper.testHelper();
    }
    
    public void beforeDelete() {
        System.debug('LiveChatTranscript (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('LiveChatTranscript (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('LiveChatTranscript (After Update)');
    }
    
    public void afterDelete() {
        System.debug('LiveChatTranscript (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('LiveChatTranscript (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.LiveChatTranscriptTriggerEnabled);
    }
}