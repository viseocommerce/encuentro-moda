public class OpportunityTriggerDispatcher implements TriggerDispatcher{
    private OpportunityHelper helper;
    public OpportunityTriggerDispatcher() {
        helper = new OpportunityHelper();
    }
    public void bulkBefore() {
        System.debug('Opportunity (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Opportunity (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Opportunity (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Opportunity (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Opportunity (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Opportunity (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Opportunity (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Opportunity (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Opportunity (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Opportunity (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.OpportunityTriggerEnabled);
    }
}