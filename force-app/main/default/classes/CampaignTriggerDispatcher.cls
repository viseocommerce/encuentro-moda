public class CampaignTriggerDispatcher implements TriggerDispatcher {
	private CampaignHelper helper;
    public CampaignTriggerDispatcher() {
        helper = new CampaignHelper();
    }
    public void bulkBefore() {
        System.debug('Campaign (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Campaign (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Campaign (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Campaign (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Campaign (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Campaign (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Campaign (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Campaign (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Campaign (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Campaign (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.CampaignTriggerEnabled);
    }
}