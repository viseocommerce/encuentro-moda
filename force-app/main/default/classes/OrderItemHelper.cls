public class OrderItemHelper {
	private List<OrderItem> oldList; 
    private List<OrderItem> newList; 
    private Map<id, OrderItem> oldMap;
    private Map<id, OrderItem> newMap;
    
    public OrderItemHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, OrderItem>) Trigger.oldMap;
        this.newMap = (Map<id, OrderItem>) Trigger.newMap;
    }
}