@isTest
public class TriggerArchitectureTest {
	@testSetup
    public static void setup() {
        TestFactory.createTestData();
    }
    
    @isTest
    public static void testAccounts() {
		List<Account> accountList = [SELECT id FROM account];
        update accountList;
        delete accountList;
        undelete accountList;
    }
    
    @isTest
    public static void testOpportunity() {
		List<Opportunity> opportunityList = [SELECT id FROM Opportunity];
        update opportunityList;
        delete opportunityList;
        undelete opportunityList;
    }
    
    @isTest
    public static void testCampaigns() {
		List<Campaign> campaignList = [SELECT id FROM Campaign];
        update campaignList;
        delete campaignList;
        undelete campaignList;
    }
    
    @isTest
    public static void testContacts() {
		List<Contact> conList = [SELECT id FROM Contact];
        update conList;
        delete conList;
        undelete conList;
    }
    
    @isTest
    public static void testCampaignMembers() {
		List<CampaignMember> cmList = [SELECT id FROM CampaignMember];
        update cmList;
        delete cmList;
    }
    
    @isTest
    public static void testCase() {
		List<Case> caseList = [SELECT id FROM Case];
        update caseList;
        delete caseList;
        undelete caseList;
    }
    
    @isTest
    public static void testContracts() {
		List<Contract> contractList = [SELECT id FROM Contract];
        update contractList;
        delete contractList;
        undelete contractList;
    }
    
    @isTest
    public static void testEvents() {
		List<Event> eventList = [SELECT id FROM Event];
        update eventList;
        delete eventList;
        undelete eventList;
    }
    
    @isTest
    public static void testLeads() {
		List<Lead> leadList = [SELECT id FROM Lead];
        update leadList;
        delete leadList;
        undelete leadList;
    }
    
    @isTest
    public static void testNotes() {
		List<Note> noteList = [SELECT id FROM Note];
        update noteList;
        delete noteList;
        undelete noteList;
    }
    
    @isTest
    public static void testOrders() {
		List<Order> orderList = [SELECT id FROM Order WHERE status = 'Draft'];
        update orderList;
        orderList = [SELECT id FROM Order];
        delete orderList;
        undelete orderList;
    }
    
    @isTest
    public static void testOrderItems() {
		List<OrderItem> orderItemList = [SELECT id FROM OrderItem];
        update orderItemList;
        delete orderItemList;
    }
    
    @isTest
    public static void testProducts2() {
		List<Product2> productList = [SELECT id FROM Product2];
        update productList;
        productList = [SELECT id FROM Product2 WHERE id NOT IN (SELECT product2Id FROM orderitem WHERE product2Id <> null)];
        delete productList;
        undelete productList;
    }
    
    @isTest
    public static void testPricebooks2() {
		List<Pricebook2> pricebook2List = [SELECT id FROM Pricebook2];
        update pricebook2List;
        delete pricebook2List;
        undelete pricebook2List;
    }
}