public class AddressTriggerDispatcher implements TriggerDispatcher{
    private AddressHelper helper;
    public AddressTriggerDispatcher() { 
        helper = new AddressHelper(); 
    }
    public void bulkBefore() {
        System.debug('Address (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Address (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Address (Finally)');
    }
    
    public void beforeInsert() {
        helper.setAddressForCustumer();
        System.debug('Address (Before Insert)');
    }
    
    public void beforeUpdate() {
        helper.updateAddressForCustumer();
        System.debug('Address (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Address (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Address (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Address (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Address (After Delete)');
    }
    
    public void afterUndelete() {
        System.debug('Address (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.AddressTriggerEnabled);
    }
}