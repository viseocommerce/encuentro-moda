@isTest
public class TestFactory {
    @isTest
    public static void createTestData() {
        Test.startTest();
        List<Account> accList = createAccounts(20);
        List<Opportunity> oppList = createOpportunities(accList, 0, 3, 30);
        List<Campaign> camList = createCampaigns(25);
        List<Contact> conList = createContacts(30);
        List<Contact> accConList = createAccountContacts(accList, 0, 3);
        List<CampaignMember> cmList = createCampaignMembers(camList, conList, 0, 5);
        List<Case> caseList = createCases(10);
        List<Contract> contractList = createContracts(accList, 0, 5);
        List<Event> eventList = createEvents(oppList, 0, 5);
        List<Lead> leadList = createLeads(30);
        List<Note> noteList = createNotes(oppList, 0, 5);
        List<Order> orderList = createOrders(contractList, 0, 5);
        List<Product2> productList = createProducts(50);
        List<Pricebook2> pricebook2List = createPricebooks(5);
        List<PricebookEntry> pbeList = createPriceBookEntries(productList);
        List<OrderItem> orderItemList = createOrderItems(orderList, pbeList, 0, 3);
        Test.stopTest();
    }
    
    public static List<Account> createAccounts(Integer numberOfAccounts) {
        List<Account> accList = new List<Account>();
        Account acc;
        
        for(Integer i = 0; i < numberOfAccounts; i++) {
        	acc = new Account();
            
            acc.Name = 'Test Account ' + i;
            
            accList.add(acc);
        }
        
        if(!accList.isEmpty())
            Database.insert(accList);
        
        return accList;
    }
    
	public static List<Opportunity> createOpportunities(List<Account> accList, Integer minNumberOfOpportunities, Integer maxNumberOfOpportunities, Integer closeDateLimitDays) {
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp;
        
        for(Account acc: accList) {
            Double d = math.random() * maxNumberOfOpportunities;
			Integer n = d.intValue() + minNumberOfOpportunities;
            
            for(Integer i = 0; i < n; i++) {
                opp = new Opportunity();
                
                opp.Name = 'Test Opportunity ' + acc.Name + ' ' + i;
                opp.CloseDate = Date.today().addDays((math.random() * closeDateLimitDays).intValue());
                
                switch on ((math.random() * 3).intValue()) {
                    when 0 {
                        opp.StageName = 'Prospecting';
                    }
                    when 1 {
                        opp.StageName = 'Qualification';
                    }
                    when 2 {
                        opp.StageName = 'Needs Analysis';
                    }
                    when 3 {
                        opp.StageName = 'Prospecting';
                    }
                    when else {
                        opp.StageName = 'Closed Won';
                    }
                }
                oppList.add(opp);
            }
        }
        
        if(!oppList.isEmpty())
            Database.insert(oppList);
        
        return oppList;
    }    

	public static List<Campaign> createCampaigns(Integer numberOfCampaigns) {
        List<Campaign> campaignList = new List<Campaign>();
        Campaign cmp;
	
        for(Integer i = 0; i < numberOfCampaigns; i++) {
            cmp = new Campaign();
            cmp.Name = 'Test Campaign ' + i;
            campaignList.add(cmp);
        }
        
        if(!campaignList.isEmpty())
            Database.insert(campaignList);
        
        return campaignList;
    }    

    public static List<Contact> createContacts(Integer numberOfContacts) {
        List<Contact> conList = new List<Contact>();
        Contact con;
        
        for(Integer i = 0; i < numberOfContacts; i++) {
            con = new Contact();
            con.LastName = 'Test Contact ' + i;
            conList.add(con);
        }
        
        if(!conList.isEmpty())
            Database.insert(conList);
        
        return conList;
    }
    
    public static List<Contact> createAccountContacts(List<Account> accList, Integer minNumberOfContacts, Integer maxNumberOfContacts) {
        List<Contact> conList = new List<Contact>();
        Contact con;
        
        for(Account acc: accList) {
            Double d = math.random() * maxNumberOfContacts;
			Integer n = d.intValue() + minNumberOfContacts;
            for(Integer i = 0; i < n; i++) {
                con = new Contact();
            	con.LastName = 'Test Contact ' + acc.Name + ' ' + i;
                conList.add(con);
            }
        }      
        
        if(!conList.isEmpty())
            Database.insert(conList);
        
        return conList;
    }
    
    public static List<CampaignMember> createCampaignMembers(List<Campaign> camList, List<Contact> conList, Integer minNumberOfCampaignMembers, Integer maxNumberOfCampaignMembers) {
        List<CampaignMember> cmList = new List<CampaignMember>();
        CampaignMember cm;
        Set<Id> contactIdSet = new Set<Id>();
        Integer n, r;
        Double d;
        Id contactId;
        
        for(Campaign cam: camList) {
            d = math.random() * maxNumberOfCampaignMembers;
			n = d.intValue() + minNumberOfCampaignMembers;
            contactIdSet = new Set<Id>();
            
            for(Integer i = 0; i < n; i++) {
                cm = new CampaignMember();
                cm.CampaignId = cam.Id;
                
                r = (math.random() * conList.size()).intValue();
                contactId = conList.get(r).Id;
                
                Do {
                    r = (math.random() * conList.size()).intValue();
                	contactId = conList.get(r).Id;
                } while(contactIdSet.contains(contactId));
                contactIdSet.add(contactId);
            	cm.ContactId = contactId;
                
				                
                cmList.add(cm);
            }
        }      
        
        if(!cmList.isEmpty())
            Database.insert(cmList);
        
        return cmList;
    }
    
    public static List<Case> createCases(Integer numberOfCases) {
        List<Case> caseList = new List<Case>();
        Case c;
        
        for(Integer i = 0; i < numberOfCases; i++) {
        	c = new Case();
            switch on ((math.random() * 3).intValue()) {
                    when 0 {
                        c.Status = 'New';
                    }
                    when 1 {
                        c.Status = 'Working';
                    }
                    when 2 {
                        c.Status = 'Escalated';
                    }
                    when else {
                        c.Status = 'Closed';
                    }
            }
            
            switch on ((math.random() * 3).intValue()) {
                    when 0 {
                        c.Origin = 'Phone';
                    }
                    when 1 {
                        c.Origin = 'Email';
                    }
                    when else {
                        c.Origin = 'Web';
                    }
            }
            caseList.add(c);
        }
        
        if(!caseList.isEmpty())
            Database.insert(caseList);
        
        return caseList;
    }
    
    public static List<Contract> createContracts(List<Account> accList, Integer minNumberOfContracts, Integer maxNumberOfContracts) {
        List<Contract> contractList = new List<Contract>();
        Contract con;
        
        for(Account acc: accList) {
			Double d = math.random() * maxNumberOfContracts;
			Integer n = d.intValue() + minNumberOfContracts;
            
            for(Integer i = 0; i < n; i++) {
        		con = new Contract();
                con.AccountId = acc.Id;
                con.StartDate = Date.today();
                con.ContractTerm = (30);
            	con.Status = 'Draft';
            
            	contractList.add(con);
        	}
        }
        
        
        if(!contractList.isEmpty())
            Database.insert(contractList);
        
        return contractList;
    }
    
    public static List<Event> createEvents(List<Opportunity> oppList, Integer minNumberOfEvents, Integer maxNumberOfEvents) {
        List<Event> eventList = new List<Event>();
		Event e;
        
        for(Opportunity opp: oppList) {
			Double d = math.random() * maxNumberOfEvents;
			Integer n = d.intValue() + minNumberOfEvents;
            
            for(integer i = 0; i < n; i++) {
            	e = new Event();
				e.OwnerId = UserInfo.getUserId();
                e.Subject = 'Test Event ' + opp.Id + ' ' + i;
                e.StartDateTime = Date.today();
                e.EndDateTime = Date.today();                
                eventList.add(e);
            }
            
        }
		
        if(!eventList.isEmpty())
        	Database.insert(eventList);
        
        return eventList;
    }
    
    public static List<Lead> createLeads(Integer numberOfLeads) {
        List<Lead> leadList = new List<Lead>();
        Lead l;
        
        for(Integer i = 0; i < numberOfLeads; i++) {
        	l = new Lead();
            
            l.LastName = 'Test ' + i;
            l.Company = 'Test Lead Company ' + i;
            switch on ((math.random() * 3).intValue()) {
                    when 0 {
                        l.Status = 'Open - Not Contacted';
                    }
                    when 1 {
                        l.Status = 'Working - Contacted';
                    }
                    when else {
                        l.Status = 'Closed - Not Converted';
                    }
            }
            
            leadList.add(l);
        }
        
        if(!leadList.isEmpty())
            Database.insert(leadList);
        
        return leadList;
    }
    
    public static List<Note> createNotes(List<Opportunity> oppList, Integer minNumberOfNotes, Integer maxNumberOfNotes) {
        List<Note> noteList = new List<Note>();
		Note note;
        
        for(Opportunity opp: oppList) {
			Double d = math.random() * maxNumberOfNotes;
			Integer n = d.intValue() + minNumberOfNotes;
            
            for(integer i = 0; i < n; i++) {
            	note = new Note();
                note.ParentId = opp.Id;
                note.Title = 'Test Note ' + i;
				note.Body = 'Test body ' + i;       
                noteList.add(note);
            }
            
        }
		
        if(!noteList.isEmpty())
        	Database.insert(noteList);
        
        return noteList;
    }
    
    public static List<Pricebook2> createPricebooks(Integer numberOfPricebooks) {
        List<Pricebook2> pricebookList = new List<Pricebook2>();
		Pricebook2 pricebook;
        
        for(Integer i = 0; i < numberOfPricebooks; i++) {
            pricebook = new Pricebook2();
            
            pricebook.Name = 'Test Pricebook ' + i;
            pricebook.IsActive = True;
            pricebook.Description = 'Test Pricebook Description ' + i;
            
            pricebookList.add(pricebook);
        }

		if(!pricebookList.isEmpty()) 
            Database.insert(pricebookList);
        
        return pricebookList;
    }
    
    public static List<Order> createOrders(List<Contract> contractList, Integer minNumberOfOrders, Integer maxNumberOfOrders) {
        List<Order> orderList = new List<Order>();
        Set<Id> accountIdSet = new Set<Id>();
        Order o;    
        
        for(Contract c: contractList) {
            Double d = math.random() * maxNumberOfOrders;
			Integer n = d.intValue() + minNumberOfOrders;
            if(!accountIdSet.contains(c.AccountId)) {
                for(integer i = 0; i < n; i++) {
                	o = new Order();
                    o.AccountId = c.AccountId;
                    o.ContractId = c.Id;
                    o.EffectiveDate = Date.today().addDays(15);
                    o.Status = 'Draft';
                    o.Pricebook2Id = Test.getStandardPricebookId();
                    orderList.add(o);
            	}
                accountIdSet.add(c.AccountId);
            }
        }
        
        if(!orderList.isEmpty())
            Database.insert(orderList);
        
        return orderList;
    }
    
    public static List<PricebookEntry> createPriceBookEntries(List<Product2> productList) {
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        PricebookEntry pbe;
        
        for(Product2 p: productList) {
        	pbe = new PricebookEntry();
            
            pbe.Product2Id = p.Id;
            pbe.IsActive = True;
            pbe.UnitPrice = 1;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
         
            pricebookEntryList.add(pbe);
        }
        
        if(!pricebookEntryList.isEmpty())
            Database.insert(pricebookEntryList);
        
        return pricebookEntryList;
    }
    public static List<Product2> createProducts(Integer numberOfProducts) {
        List<Product2> productList = new List<Product2>();
        Product2 product;
        
        for(Integer i=0; i<numberOfProducts; i++) {
            product = new Product2();
			product.Name = 'Test Product ' + i;
			product.IsActive = True;
			product.Description = 'Test product description ' + i;           
            productList.add(product);
        }
        
        if(!productList.isEmpty())
            Database.insert(productList);
        
    	return productList;    
    }
    
    public static List<OrderItem> createOrderItems(List<Order> orderList, List<PricebookEntry> pbeList, Integer minNumberOfOrderItems, Integer maxNumberOfOrderItemes) {
        List<OrderItem> oiList = new List<OrderItem>();
        Set<Id> pricebookEntryIdSet;
        OrderItem oi;
        PricebookEntry pbe;
        
        for(Order o: orderList) {
            pricebookEntryIdSet = new Set<Id>();
            Double d = math.random() * maxNumberOfOrderItemes;
			Integer n = d.intValue() + minNumberOfOrderItems;
			
            for(Integer i = 0; i < n; i++) {
				oi = new OrderItem();
                oi.OrderId = o.Id;
                
                do {
                    pbe = pbeList.get((math.random() * pbeList.size()).intValue());
                } while(pricebookEntryIdSet.contains(pbe.id));
                pricebookEntryIdSet.add(pbe.id);
                
                oi.Product2Id = pbe.Product2Id;
                oi.PricebookEntryId = pbe.Id;
                oi.UnitPrice = 1;
                oi.Quantity = (math.random() * 50).intValue() + 1;
            	oiList.add(oi);                
            }
        }
        
        if(!oiList.isEmpty())
            Database.insert(oiList);
        
        return oiList;
    }
    
}