public class IntegrationLogUtility { 
    @testVisible
    private static Boolean testFutureMethod = false;

    public enum Module {
        Siebel,
        SAP
    }

    public enum Direction {
        Inbound,
        Outbound
    }

    public enum Method {
        GET,
        POST,
        PUT,
        PATCH
    }

    @future
    public static void createLogAsync(String module, String direction, String endpoint, String method, String requestBody, String responseBody, Integer responseStatusCode, String errorMessage) {
        publishLog(module, direction, endpoint, method, requestBody, responseBody, responseStatusCode, errorMessage);
    }

    public static void createLogSync(String module, String direction, String endpoint, String method, String requestBody, String responseBody, Integer responseStatusCode, String errorMessage) {
        publishLog(module, direction, endpoint, method, requestBody, responseBody, responseStatusCode, errorMessage);
    }

    public static void createLog(String module, String direction, String endpoint, String method, String requestBody, String responseBody, Integer responseStatusCode, String errorMessage) {
        if(!System.isFuture() && !testFutureMethod)
            createLogAsync(module, direction, endpoint, method, requestBody, responseBody, responseStatusCode, errorMessage);
        else {
            createLogSync(module, direction, endpoint, method, requestBody, responseBody, responseStatusCode, errorMessage);
        }
    }

    public static Integration_Log_Event__e newLog(String module, String direction) {
        Integration_Log_Event__e e = new Integration_Log_Event__e (
            Module__c = module,
            Direction__c = direction
        );

        return e;
    }

    public static void includeHttpRequest(Integration_Log_Event__e e, HttpRequest request) {
        e.Endpoint__c = request.getEndpoint();
        e.Method__c = request.getMethod();
        e.Request_Body__c = request.getBody();
    }

    public static void includeHttpResponse(Integration_Log_Event__e e, HttpResponse response) {
        e.Response_StatusCode__c = String.valueOf(response.getStatusCode());
        e.Response_Body__c = response.getBody();
    }

    public static void includeRestContext(Integration_Log_Event__e e) {
        e.Endpoint__c = RestContext.request.requestURI;
        e.Method__c = RestContext.request.httpMethod;
        e.Request_Body__c = RestContext.request.requestBody.toString();

        e.Response_StatusCode__c = String.valueOf(RestContext.response.statusCode);
        e.Response_Body__c = RestContext.response.responseBody.toString();
    }

    private static List<Integration_Log_Event__e> logs = new List<Integration_Log_Event__e>();

    public static void publishLog(Integration_Log_Event__e e) {
        publishLog(e, false);
    }

    public static void publishLog(Integration_Log_Event__e e, Boolean differ) {
        if(differ) {
            logs.add(e);
        }
        else {
            EventBus.publish(e);
        }
    }

    public static void publishPending() {
        if(!logs.isEmpty()) EventBus.publish(logs);
    }

    private static void publishLog(String module, String direction, String endpoint, String method, String requestBody, String responseBody, Integer responseStatusCode, String errorMessage) {
        Integration_Log_Event__e e = new Integration_Log_Event__e (
            Module__c = module,
            Endpoint__c = endpoint,
            Method__c = method,
            Direction__c = direction,
            Request_Body__c = requestBody,
            Response_Body__c = responseBody,
            Response_StatusCode__c = String.valueOf(responseStatusCode),
            Error_Message__c = errorMessage
        );

        EventBus.publish(e);
    }

    public static void saveIntegrationLogs(List<Integration_Log_Event__e> integrationLogEvents) {
        Map<String,Integration_Log_Setting__mdt> moduleToSettings = getSettingsByModule();

        List<Integration_Log__c> integrationLogs = new List<Integration_Log__c>();

        for(Integration_Log_Event__e e : integrationLogEvents) {
            String module = String.isBlank(e.Module__c) ? '*' : e.Module__c;
            Integration_Log_Setting__mdt setting = moduleToSettings.get(module);
            if(setting == null) {
                setting = moduleToSettings.get('*');
            }
            if(setting == null) continue;

            if(String.isBlank(e.Error_Message__c) && !setting.Log_Success__c) continue;

            Integration_Log__c log = convertLog(e);
            integrationLogs.add(log);
        }

        if(!integrationLogs.isEmpty()) {
            insert integrationLogs;
        }
    }

    private static Integration_Log__c convertLog(Integration_Log_Event__e eventLog) {
        return new Integration_Log__c(
            Module__c = eventLog.Module__c,
            Endpoint__c = eventLog.Endpoint__c,
            Method__c = eventLog.Method__c,
            Direction__c = eventLog.Direction__c,
            Request_Body__c = eventLog.Request_Body__c,
            Response_Body__c = eventLog.Response_Body__c,
            Response_StatusCode__c = eventLog.Response_StatusCode__c,
            Error_Message__c = eventLog.Error_Message__c,
            Successful__c = String.isBlank(eventLog.Error_Message__c)
        );
    }

    public static String buildIntegrationLogExpirationQuery() {
        List<Integration_Log_Setting__mdt> settings = selectSettingsForExpirations();

        String whereCondition = 'WHERE ';

        for(Integration_Log_Setting__mdt setting : settings) {
            if(String.isBlank(setting.Module__c)) continue;

            Integer expirationDays = Integer.valueOf(setting.Expiration_Days__c);
            expirationDays -= 1;

            Date paramDate = Date.today().addDays(-1 * expirationDays);
            DateTime dt = Datetime.newInstance(paramDate.Year(), paramDate.Month(), paramDate.Day());
            String soqlDate = dt.format('YYYY-MM-dd\'T\'hh:mm:ss\'Z\'');

            String currentCondition = '(Module__c = \'' + setting.Module__c + '\' AND ';
            currentCondition += 'CreatedDate < ' + soqlDate;

            currentCondition += ') OR ';

            whereCondition += currentCondition;
        }

        if(whereCondition.endsWith('OR '))
            whereCondition = whereCondition.removeEnd('OR ');
        else // no settings were found
            whereCondition += 'Id = null'; // do not return anything

        String result = 'select Id from Integration_Log__c ' + whereCondition;

        return result;
    }

    private static Map<String,Integration_Log_Setting__mdt> getSettingsByModule() {
        Map<String,Integration_Log_Setting__mdt> moduleToSetting = new Map<String,Integration_Log_Setting__mdt>();

        for(Integration_Log_Setting__mdt setting : selectSettings()) {
            String module = setting.Module__c;
            if(String.isBlank(module)) module = '*';
            moduleToSetting.put(module,setting);
        }

        return moduleToSetting;
    }

    // SELECTORS

    private static List<Integration_Log_Setting__mdt> selectSettings() {
        return [
            select
                Id,
                Module__c,
                Log_Success__c
            from
                Integration_Log_Setting__mdt
            where
                Disabled__c = false
        ];
    }

    private static List<Integration_Log_Setting__mdt> selectSettingsForExpirations() {
        return [
            select
                Id,
                Expiration_Days__c,
                Module__c
            from
                Integration_Log_Setting__mdt
            where
                Expires__c = true and
                Expiration_Days__c != null and
                Disabled__c = false
        ];
    }
}