global class CaseEmailTemplateSelector implements Support.EmailTemplateSelector {
    
    // Empty constructor
    global CaseEmailTemplateSelector() {  }
    
    global ID getDefaultEmailTemplateId(ID caseId) {
    
    EmailTemplate et;
    
        // use the Avery email template
        et = [SELECT Id FROM EmailTemplate WHERE Name = 'Como Comprar - Países'];
        
        return et.id;
    
    }

}