public class Pricebook2TriggerDispatcher implements TriggerDispatcher{
    private Pricebook2Helper helper;
    public Pricebook2TriggerDispatcher() {
        helper = new Pricebook2Helper();
    }
    public void bulkBefore() {
        System.debug('PricebookTrigger (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('PricebookTrigger (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('PricebookTrigger (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('PricebookTrigger (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('PricebookTrigger (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('PricebookTrigger (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('PricebookTrigger (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('PricebookTrigger (After Update)');
    }
    
    public void afterDelete() {
        System.debug('PricebookTrigger (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('PricebookTrigger (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.Pricebook2TriggerTriggerEnabled);
    }
}