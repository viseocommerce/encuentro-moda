public class Payment_InformationHelper {
	private List<Payment_Information__c> oldList; 
    private List<Payment_Information__c> newList; 
    private Map<id, Payment_Information__c> oldMap;
    private Map<id, Payment_Information__c> newMap;
    
    public Payment_InformationHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Payment_Information__c>) Trigger.oldMap;
        this.newMap = (Map<id, Payment_Information__c>) Trigger.newMap;
    }

    public void avoidDuplicateOrders() {
        List<Payment_Information__c> cardsToDelete = new List<Payment_Information__c>();
        List<id> ordersIds = new List<id>();
        List<String> cardNums = new List<String>();
        for(Payment_Information__c cardRecord: this.newList) {
            ordersIds.add(cardRecord.Order__c);
            cardNums.add(cardRecord.Card_Number__c);
        }
        List<Payment_Information__c> cardRecords =[SELECT id,Order__c,Card_Number__c FROM Payment_Information__c
                                                WHERE Order__c IN:ordersIds AND Card_Number__c IN:cardNums
                                                ORDER BY Card_Number__c];

        String orderId ;
        String cardNum ;
        for (Payment_Information__c cardRecord : cardRecords) {
            if(cardRecord.Order__c == orderId && cardRecord.Card_Number__c == cardNum){
                cardsToDelete.add(cardRecord);
            }
            orderId = cardRecord.Order__c;
            cardNum = cardRecord.Card_Number__c;
        }
        System.debug('cards to delete :: '+ cardsToDelete);
        delete cardsToDelete;
    }
}