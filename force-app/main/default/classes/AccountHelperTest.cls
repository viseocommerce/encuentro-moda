@isTest
public class AccountHelperTest {

    @testSetup
    static void setup() {
        SCCTestDataFactory.createAccount(new Map<Integer, String>{1=>'fname', 2=>'fname', 3=>'fname'}, new Map<Integer, String>{1=>'lname', 2=>'lname', 3=>'lname'}, new Map<Integer, String>{1=>'test@hotmail.com', 2=>'test@yahoo.com', 3=>'test@salesforce.com'}, new Map<Integer, String>{1=>'12345', 2=>'23456', 3=>'34567'});       
    }

    @isTest
    static void test_continueWithObject_one(){
        Test.startTest();
        List<Account> acc = [SELECT id, Lastname, Apellido1_CZZ__c FROM Account LIMIT 1];
        //acc[0].LastName ='';
        acc[0].Apellido1_CZZ__c ='TEST';
        acc[0].PersonEmail ='test@test.com';
        acc[0].Movil_CZZ__c ='1234578';
        update acc;
        acc[0].FirstName ='TEST2';
        acc[0].EmailComerce__c ='test2@test.com';
        acc[0].Phone ='12345789';
        acc[0].Ac_pol_tratamiento_de_datos_CZZ__c =false;
        acc[0].Acepta_notificaciones_comerciales_CZZ__c =true;
        acc[0].SFCC_Customer_Number__pc ='';
        acc[0].Numero_fidelizado_CZZ__c ='';
        update acc;

        delete acc;
        Test.stopTest();
    }

    @isTest
    static void updateAddress_BeforeUpdate(){
        Test.startTest();
        List<Account> acc = [SELECT id FROM Account LIMIT 1];
        Address__c testAddress = new Address__c(Account__c = acc[0].id,
                                                Address_Line_1__c = 'calle Test 12',
                                                City__c = 'Barcelona',
                                                Country__c = 'ES',
                                                from_SFCC__c = true,
                                                Sync_Address__c = true);
        insert testAddress;                                                 
        Test.stopTest();
        Account resultAccount =[SELECT id, Nombre_de_via_CZZ__c FROM Account WHERE id=:acc[0].id];

        System.assertEquals(testAddress.Address_Line_1__c, resultAccount.Nombre_de_via_CZZ__c, 'Account was not changed');
    }
    @isTest
    static void updateAddressInAccount_BeforeUpdate(){
        Test.startTest();
        List<Account> acc = [SELECT id,Nombre_de_via_CZZ__c FROM Account LIMIT 1];
        Address__c testAddress = new Address__c(Account__c = acc[0].id,
                                                Address_Line_1__c = 'calle Test 12',
                                                City__c = 'Barcelona',
                                                Country__c = 'ES',
                                                from_SFCC__c = true,
                                                Sync_Address__c = true);
        insert testAddress;
        acc[0].Nombre_de_via_CZZ__c='calle Test 13';
        update acc;
        testAddress.Address_Line_1__c= 'calle Test 14';
        update testAddress;                                                 
        Test.stopTest();
        Account resultAccount =[SELECT id, Nombre_de_via_CZZ__c FROM Account WHERE id=:acc[0].id];

        System.assertEquals(testAddress.Address_Line_1__c, resultAccount.Nombre_de_via_CZZ__c, 'Address was not changed');
    }
}