public class AccountTriggerDispatcher implements TriggerDispatcher{
    private AccountHelper helper;
    public AccountTriggerDispatcher() { 
        helper = new AccountHelper(); 
    }
    public void bulkBefore() {
        System.debug('Account (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Account (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Account (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Account (Before Insert)');
        helper.setAnualIncome_BeforeInsert();
        helper.setFields_BeforeInsert();
        helper.avoidDuplicateEmails_BeforeInsert();
    }
    
    public void beforeUpdate() {
        helper.setFields_BeforeInsert();
        helper.updateAddress_BeforeUpdate();
        System.debug('Account (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Account (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Account (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Account (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Account (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Account (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.AccountTriggerEnabled);
    }
}