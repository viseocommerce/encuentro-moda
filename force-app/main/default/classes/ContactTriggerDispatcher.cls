public class ContactTriggerDispatcher implements TriggerDispatcher {
    private ContactHelper helper;
    public ContactTriggerDispatcher() {
        helper = new ContactHelper();
    }
    public void bulkBefore() {
        System.debug('Contact (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Contact (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Contact (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Contact (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Contact (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Contact (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Contact (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Contact (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Contact (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Contact (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.ContactTriggerEnabled);
    }
}