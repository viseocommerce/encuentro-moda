public class CampaignHelper {
	private List<Campaign> oldList; 
    private List<Campaign> newList; 
    private Map<id, Campaign> oldMap;
    private Map<id, Campaign> newMap;
    
    public CampaignHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Campaign>) Trigger.oldMap;
        this.newMap = (Map<id, Campaign>) Trigger.newMap;
    }
}