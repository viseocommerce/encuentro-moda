public class AccountComerzia {
    
    public AccountData DATOS;

    public class AccountData {
        public String Nombre;
        public String Apellido1;
        public String Apellido_2;
        public String Tipo_de_via;
        public String Nombre_de_via;
        public String Numero;
        public String Complemento;
        public String Codigo_postal;
        public String Localidad;
        public String Poblacion;
        public String Provincia;
        public String Pais;
        public String Tipo_de_documento;
        public String N_documento;
        public String Nacionalidad;
        public String Fecha_de_nacimiento;
        public String Sexo;
        public String Estado_Civil;
        public String Observaciones;
        public String Activo;
        public String Fecha_de_alta_Comerzzia;
        public String Editable_Comerzzia;
        public String Motivo_de_baja_Comerzzia;
        public String Id_de_cliente_Comerzzia;
        public String Tienda_Captacion;
        public String Id_Usuario_Alta;
        public String Id_Usuario_Baja;
        public String Numero_fidelizado;
        public String Grupo_fidelizado;
        public String Ac_pol_tratamiento_de_datos;
        public String Acepta_notificaciones_comerciales;
        public String Fe_trat_datos;
        public String Movil;
        public String Permitido_contacto_por_Mail;
        public String Permitido_contacto_por_telefono;
        public String Cod_Tipo_Contacto;
        public String Valor;
        public String Recibe_Notificaciones;
        public String Recibe_Notificaciones_Comerciales;
        public String Id_Tarjeta;
        public String Numero_de_tarjeta;
        public String Id_Cuenta_Tarjeta;
        public String Fecha_Emision;
        public String Fecha_Activacion;
        public String Fecha_Ultimo_uso;
        public String Fecha_baja;
        public String Fecha_asignacin_fidelizado;
        public String Cod_Tipo_Tarjeta;
        public String Id_Clase;
        public String Id_Objeto;
        public String IdSalesforce;
        public String EmailComerce;
        public String Segmento;

    }
   
}