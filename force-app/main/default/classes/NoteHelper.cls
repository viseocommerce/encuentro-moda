public class NoteHelper {
	private List<Note> oldList; 
    private List<Note> newList; 
    private Map<id, Note> oldMap;
    private Map<id, Note> newMap;
    
    public NoteHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Note>) Trigger.oldMap;
        this.newMap = (Map<id, Note>) Trigger.newMap;
    }
}