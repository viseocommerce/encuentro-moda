public class SandBox {
    //Centro Penitenciario, Cooperativa, Distribuidor, Grandes Superficies, Hospitales, Organismos y Relaciones Institucionales
        //
        ////////////////////////////////////
        //////////////////////////////////
        /// /// /// RETRIEVE/// /// /// /// 
        //////////////////////////////////
        //////////////////////////////////
        //
        //
        //
    @AuraEnabled
    public static List<BusinessConditionsDTO> getBusinessConditionsMethod(){
        System.debug('jsonPetition1 : ');
        //ALL LIST
        List<BusinessConditionsDTO> listEconCond = new List<BusinessConditionsDTO>();
        //FILTER LIST
        List<BusinessConditionsDTO> listEconCondFilter = new List<BusinessConditionsDTO>();
        //Sorted LIST
        List<BusinessConditionsDTO> listEconCondSorted = new List<BusinessConditionsDTO>();
        //Size List
        List<BusinessConditionsDTO> listEconCondSized = new List<BusinessConditionsDTO>();
        /////Acc
        /*Account actualAcc =  [SELECT Organizaci_n_de_ventas__c,Sector__c,Canal_de_distribuci_n__c,C_digo_SAP_S4__c,Codigo_SAP__c
                                    from account 
                                    where id =: identifier];*/
        
        //C_digo_SAP_S4__c // Codigo_SAP__c - 10 caracters
        String actualCode = '0005900314';
        String resultCode = actualCode;
        if(actualCode.length() < 10){
            //system.debug(n.length());
            if(actualCode.length() == 1){
                resultCode = '000000000'+actualCode;
            } 
            if(actualCode.length() == 2){
                resultCode = '00000000'+actualCode;
            }  
            if(actualCode.length() == 3){
                resultCode = '0000000'+actualCode;
            }
            if(actualCode.length() == 4){
                resultCode = '000000'+actualCode;
            }
            if(actualCode.length() == 5){
                resultCode = '00000'+actualCode;
            }
            if(actualCode.length() == 6){
                resultCode = '0000'+actualCode;
            }
            if(actualCode.length() == 7){
                resultCode = '000'+actualCode;
            }
            if(actualCode.length() == 8){
                resultCode = '00'+actualCode;
            }
            if(actualCode.length() == 9){
                resultCode = '0'+actualCode;
            }
        }

        //ORG
         String actualOrg = 'Kern Pharma';
        String org;//KES1 o KES3
        String centroSuministrador; // ???? K010 (Actafarma)K001 (Kern) MOD TEST
        if(actualOrg == 'Kern Pharma'){
            org = 'KES1';
            centroSuministrador = 'K001';
        }
        else if(actualOrg == 'Actafarma'){
            org = 'KES3';
            centroSuministrador  = 'K010';
        }
        else{
            org = 'KES1';
            centroSuministrador = 'K001';
        }

        String canal = '01';    //01 -> Nacional / 02 -> Export
        String sector = '01';                   //Industry o Sector__c ?
        String codigoSap = '0021000015';           //C_digo_SAP_S4__c // Codigo_SAP__c
        String fechaPrecio = '20201130';//= viewDateIni;            //VAR
        String kam = 'true';// ????

        ////////////////////////////////////
        //LLAMADA SAP//////////////////////
        ////////////////////////////////////

        List<BusinessConditionsDTO> listBC;
        String jsonPetition = generateJSONretrieve(org,canal,sector,centroSuministrador,codigoSap,fechaPrecio,kam);
        System.debug('jsonPetition : ' + jsonPetition);
       

        ////////////////////////////////////
        //LLAMADA SAP//////////////////////
        ////////////////////////////////////
        listEconCond = RetrieveConditionsCallout(jsonPetition);
        
        ////////////////////////////////////
        //TEST RECORDS//////////////////////
        ////////////////////////////////////
        /* For(Integer i = 0; i < 100; i++){
            Integer nI = 11-i; 
            String nS = string.valueOf(nI);

            if(i == 3){
                BusinessConditionsDTO actualBC = new BusinessConditionsDTO( 'numMat'+i,  'precTarif',  'precTarComp', 'precFin',  'precFinComp',  'precFinImp',  'preciFinImpComp',  'tipPrecApl',  'clavMon',  'fechIniVali','fechFinVal','porcDesc','porcDtoReDecr','clienNod','nomClienNod','conc','descConcurs','agrupCond','codError','1','2','1','2');
                listEconCond.add(actualBC);
            }
            else{
                BusinessConditionsDTO actualBC = new BusinessConditionsDTO( 'numMat'+i,  'precTarif',  'precTarComp', 'precFin',  'precFinComp',  'precFinImp',  'preciFinImpComp',  'tipPrecApl',  'clavMon',  'fechIniVali','fechFinVal','porcDesc','porcDtoReDecr','clienNod','nomClienNod','conc','descConcurs','agrupCond',null,'1','2','1','2');
                listEconCond.add(actualBC);
            }
        }*/

        //IF Exist Records
        if(listEconCond.size() > 0){
            
            listEconCondSorted = listEconCond;
        }
        

        return listEconCondSorted;
    }
          ////////////////////////////////////
        //////////////////////////////////
        ///////// RETRIEVE Callout/// ////// 
        //////////////////////////////////
        //////////////////////////////////
    private static List<BusinessConditionsDTO> RetrieveConditionsCallout(String jsonPetition){
        system.debug('RetrieveConditionsCallout');
        List<BusinessConditionsDTO> valueResult = new List<BusinessConditionsDTO>();//size 0

        //LOG
        Integration_Log_Event__e log = IntegrationLogUtility.newLog('Solicitud Condiciones Comerciales',String.valueOf(IntegrationLogUtility.Direction.Outbound));

        String endpoint = 'callout:SAP/RESTAdapter/ConsultaCondicionesComerciales';//EndPoint Request

        String responseBody;//Response result
        String orderBody;//Request Body
        HttpRequest request = new HttpRequest();//Request petition
        HttpResponse response = new HttpResponse();//Response Request
        Http http = new Http();

        //Body
        orderBody = jsonPetition;
        system.debug('orderBody1' +  orderBody);
        //Replace
        orderBody = orderBody.replace('_c','__c');

        ///FORMAT JSON DEBUG
        String stringJSONdebug = orderBody.replaceAll('\"','"');
        stringJSONdebug = stringJSONdebug.replaceAll('\n',' ');
        system.debug('orderBody2: ' +  stringJSONdebug);
        ///FORMAT JSON DEBUG

        ///REQUEST
            //Request
            request.setMEthod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setEndpoint(endpoint);
            request.setBody(orderBody);
            //DEBUG REQUEST
            system.debug('Request: '+ request);
            system.debug('Request Body: '+ request.getBody());

            ////TEST////
            //Response and LOG
            /*IntegrationLogUtility.includeHttpRequest(log, request);
            response = http.send(request);
            IntegrationLogUtility.includeHttpResponse(log, response);
            responseBody = response.getBody();*/

            ///TEST///
            IntegrationLogUtility.includeHttpRequest(log, request);
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{	"DATOS": [{ "Agrupadorcondiciones": "AAA", "ClaveMoneda": "AAA", "Cliente": "AAA", "CodigoError": "AAA", "Concurso": "AAA", "DescripcionConcurso": "AAA", "FechaFin": "AAA", "FechaInicio": "AAA", "NombreCliente": "AAA", "NumeroMaterial": "000000010010001105", "PorcentajeDescuento": "AAA", "PorcentajeDescuentoComercial": "AAA", "PorcentajeDtoDecreto": "AAA", "PrecioFinal": "AAA", "PrecioFinalComprimido": "AAA", "PrecioFinalImpuesto": "AAA", "PrecioFinalImpuestoComprimido": "AAA", "PrecioTarifa": "12.5", "PrecioTarifaComprimido": "AAA", "TipoPrecio": "AAA", "UnidadesEnvase": "AAA"}, { "Agrupadorcondiciones": "BBB", "ClaveMoneda": "BBB", "Cliente": "BBB", "CodigoError": "", "Concurso": "BBB", "DescripcionConcurso": "BBB", "FechaFin": "BBB", "FechaInicio": "BBB", "NombreCliente": "BBB", "NumeroMaterial": "000000010010001001", "PorcentajeDescuento": "BBB", "PorcentajeDescuentoComercial": "BBB", "PorcentajeDtoDecreto": "BBB", "PrecioFinal": "BBB", "PrecioFinalComprimido": "BBB", "PrecioFinalImpuesto": "BBB", "PrecioFinalImpuestoComprimido": "BBB", "PrecioTarifa": "15.5", "PrecioTarifaComprimido": "BBB", "TipoPrecio": "BBB", "UnidadesEnvase": "BBB"}]}');
            response.setStatusCode(200);
            IntegrationLogUtility.includeHttpResponse(log, response);
            responseBody = response.getBody();
			
            //DEBUG RESPONSE
            System.debug('Response StatusCode: '+ response.getstatusCode());
            System.debug('Response Body: '+ response.getBody());
            //System.debug('Response Body2: ' + JSON.serialize(response.getBody().replaceAll('\\n','<br/>')));


        //Publish LOG
        system.debug(log);
        IntegrationLogUtility.publishLog(log);
        //RETURN RESULT
        return valueResult;
    }

    
    //Generate Body
    public static String generateJSONretrieve(String org, String canal, String sector, String centroSuministrador, String codigoSap, String fechaPrecio, String kam) {
        
        //datosRetrieve
        datosRetrieve datRet = new datosRetrieve(org,
        canal,
        sector,
        centroSuministrador,
        fechaPrecio,
        codigoSap,
        //idCuenta,
        kam);

        ////GENERATE JSON////
        String getStringOrder; //String that contains the Order Request JSON
        JSONGenerator gen = JSON.createGenerator(true);//Variable to create JSON

        //Open JSON
        gen.writeStartObject();

        //Write DATOS
        gen.writeFieldName('DATOS');   
        gen.writeObject(datRet);

        //Close JSON
        gen.writeEndObject();

        //Convert to String and return
        getStringOrder = gen.getAsString();//JSON that contains the Order Request
        system.debug('STRING JSON: ' + getStringOrder);
        return getStringOrder;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////CLASS//////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////DATOS a enviar para consultar////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    public class datosRetrieve{

        String Organizaci_n_de_ventas_c;
        String Canal_de_distribuci_n_c;
		String Sector_c;
		String CentroSuministrador;
		String FechaPrecio;
		String C_digo_SAP_S4_c;
		String KAM;
        
        public datosRetrieve(String newOrganizaci_n_de_ventas_c, String newCanal_de_distribuci_n_c, String newSector_c, String newCentroSuministrador, String newFechaPrecio, String newC_digo_SAP_S4_c, String newKAM){
            
            Organizaci_n_de_ventas_c = newOrganizaci_n_de_ventas_c;
            Canal_de_distribuci_n_c = newCanal_de_distribuci_n_c;
            Sector_c = newSector_c;
            CentroSuministrador = newCentroSuministrador;
            FechaPrecio = newFechaPrecio;
            C_digo_SAP_S4_c = newC_digo_SAP_S4_c;
            KAM = newKAM;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////DATOS a mostrar////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    @TestVisible
    public class BusinessConditionsDTO{
        
        @AuraEnabled public String numeroMaterial{ get;set;}//SALIDA
        @AuraEnabled public String precioTarifa{ get;set;}//SALIDA
        @AuraEnabled public String precioTarifaComprimido{ get;set;}
        @AuraEnabled public String precioFinal{ get;set;}//Mod//SALIDA
        @AuraEnabled public String precioFinalComprimido{ get;set;}
        @AuraEnabled public String precioFinalImpuesto{ get;set;}
        @AuraEnabled public String precioFinalImpuestoComprimido{ get;set;}
        @AuraEnabled public String tipoPrecioAplicado{ get;set;}
        @AuraEnabled public String claveMoneda{ get;set;}
        @AuraEnabled public String fechaInicioValidez{ get;set;}
        @AuraEnabled public String fechaFinValidez{ get;set;}
        @AuraEnabled public String porcentajeDescuento{ get;set;}//mod
        @AuraEnabled public String porcentajeDtoRealDecreto{ get;set;}
        @AuraEnabled public String clienteNodo{ get;set;}
        @AuraEnabled public String nombreClienteNodo{ get;set;}
        @AuraEnabled public String concurso{ get;set;}
        @AuraEnabled public String descConcurso{ get;set;}
        @AuraEnabled public String agrupadorCondiciones{ get;set;}
        @AuraEnabled public String codigoError{ get;set;}
        @AuraEnabled public String porcentajeDescuentoComercial{ get;set;}
        @AuraEnabled public String unidadesEnvase{ get;set;}
        //ADITIONAL FIELDS / COLUMNS
        @AuraEnabled public String CodigoNacional{ get;set;}
        @AuraEnabled public String NombreProducto{ get;set;}
        //CSS
        @AuraEnabled public String errorColor;

        
        public BusinessConditionsDTO(String numMat, String precTarif, String precTarComp, String precFin, String precFinComp, String precFinImp, String preciFinImpComp, String tipPrecApl, String clavMon, String fechIniVali, String fechFinVal, String porcDesc, String porcDtoReDecr, String clienNod, String nomClienNod, String conc, String descConcurs, String agrupCond, String codError, String porcDescCom, String unidEnva,String codNac,String nomProd){
 
            this.numeroMaterial = numMat;
            this.precioTarifa = precTarif;
            this.precioTarifaComprimido = precTarComp;
            this.precioFinal = precFin;
            this.precioFinalComprimido = precFinComp;
            this.precioFinalImpuesto = precFinImp;
            this.precioFinalImpuestoComprimido = preciFinImpComp;
            this.tipoPrecioAplicado = tipPrecApl;
            this.claveMoneda = clavMon;
            this.fechaInicioValidez = fechIniVali;
            this.fechaFinValidez = fechFinVal;
            this.porcentajeDescuento = porcDesc;
            this.porcentajeDtoRealDecreto = porcDtoReDecr;
            this.clienteNodo = clienNod;
            this.nombreClienteNodo = nomClienNod;
            this.concurso  = conc;
            this.descConcurso = descConcurs;
            this.agrupadorCondiciones = agrupCond;
            this.codigoError = codError;
            this.porcentajeDescuentoComercial = porcDescCom;
            this.unidadesEnvase = unidEnva;
            //Last Fields
            //this.fechaOperación = porcDescCom;
            //this.usuarioOperacion = unidEnva;
            //ADITIONAL FIELDS / COLUMNS
            this.CodigoNacional = codNac;
            this.NombreProducto = nomProd;


            //CSS
            if(codError != null && codError != ''){
                this.errorColor = 'red';
            }
            else{
                this.errorColor = 'black';
            }
        }
    }

}