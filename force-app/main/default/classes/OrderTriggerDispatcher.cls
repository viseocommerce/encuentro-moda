public class OrderTriggerDispatcher implements TriggerDispatcher {
private OrderHelper helper;
    public OrderTriggerDispatcher() {
        helper = new OrderHelper();
    }
    public void bulkBefore() {
        System.debug('Order (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Order (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Order (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Order (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Order (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Order (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Order (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Order (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Order (After Delete)');
    }
    
    public void afterUndelete() {
        System.debug('Order (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.OrderTriggerEnabled);
    }
}