@isTest
private class IntegrationLogCleanBatchTest {
    
    @testSetup
    private static void setup() {
        
    }
    
    @isTest
    private static void test() {
        
        Integration_Log__c newIntegration = new Integration_Log__c();
        newIntegration.Module__c = 'SAP';
        insert newIntegration;
        
        Datetime pastDays = Datetime.now().addDays(-35);
		Test.setCreatedDate(newIntegration.Id, pastDays);

         Test.startTest();
       		IntegrationLogCleanBatch obj = new IntegrationLogCleanBatch();
        	DataBase.executeBatch(obj);
         Test.stopTest();
      
    }
}