/*
*   @brief  
*   @details N/A
*   @author Viseo
*   @see IntegrationLogCleanSchedulable_test,IntegrationLogCleanBatch,IntegrationLogCleanBatch_Test

*   @version 1.0
*   @date 2020/03/31
*/
global class IntegrationLogCleanSchedulable Implements Schedulable {
	
    global void execute(SchedulableContext sc){
        //AccountTeamBatch with status Completed,Aborted or Failed Jobs
        List<AsyncApexJob> aJobsProgress = new List<AsyncApexJob>();
        aJobsProgress = [SELECT Id, status, ApexClass.Name 
                        FROM AsyncApexJob 
                        where (status!='Completed' and status!='Aborted' and status!= 'Failed')  
                        and apexClass.Name='IntegrationLogCleanBatch'];
        //All Jobs in Holding
        List<AsyncApexJob> aJobs = new  List<AsyncApexJob>();
        aJobs = [SELECT Id, status FROM AsyncApexJob where status = 'Holding'];
       
        system.debug(aJobs);
        system.debug(aJobsProgress);
        
        if(aJobs.size() < 85 && aJobsProgress.size() == 0){
            IntegrationLogCleanBatch job = new IntegrationLogCleanBatch();
            Database.executeBatch(job);  
        } 
        else{
            system.debug('Hay mas de 85 trabajos en cola');
        }  
    }
}