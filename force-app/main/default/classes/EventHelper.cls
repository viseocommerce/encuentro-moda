public class EventHelper {
	private List<Event> oldList; 
    private List<Event> newList; 
    private Map<id, Event> oldMap;
    private Map<id, Event> newMap;
    
    public EventHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Event>) Trigger.oldMap;
        this.newMap = (Map<id, Event>) Trigger.newMap;
    }
}