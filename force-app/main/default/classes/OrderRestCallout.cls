/*! 
*  @brief     
*  @details   
*  @author    Viseo.
*  @see 
*  @see 
*  @version   1.0
*  @date      16/12/2020
*/
public class OrderRestCallout {
    //@future (callout=true)
    public static integer callout(Order actualOrder,List<Order_Line_Item__c> listOrderItem, List<Shipment__c> listShipment, List<Payment_Information__c> listPayment  ) {
        
        integer result = 0;

        System.debug('INSIDE WEBSERVICE ORDER:: ' + actualOrder);
        //actualOrder        //listOrderItem        //listShipment        //listPayment  
        
        //LOG
        // Integration_Log_Event__e log = IntegrationLogUtility.newLog('Creación/Modificación pedidos - Comerzzia Pedidos',String.valueOf(IntegrationLogUtility.Direction.Outbound));
        // String endpoint = 'http://www-pre.encuentromoda.net/sap/ws/sfc2cmz_pedidoventa';// = 'callout:CREDENTIALNAME/SERVICE';//EndPoint Request
        String endpoint = Label.endPointPedidoVentaComerzzia;// = 'callout:CREDENTIALNAME/SERVICE';//EndPoint Request
		String responseBody;//Response result
        String orderBody;//Request Body
        // Blob headerValue = Blob.valueOf('usrAsteria' + ':' + 'poduser01');
        Blob headerValue = Blob.valueOf(Label.usuarioComerzzia + ':' + Label.passwordComerzzia);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        HttpRequest request = new HttpRequest();//Request petition
        HttpResponse response = new HttpResponse();//Response Request
        Http http = new Http();
        
        //GET STRING / JSON
        orderBody = outsourceGenJSON(actualOrder,listOrderItem,listShipment,listPayment);
        System.debug('JSON sent out with Account vals: '+ orderBody);
        
        ///FORMAT JSON DEBUG
        //String stringJSONdebug = orderBody.replaceAll('\"','"');
        //stringJSONdebug = stringJSONdebug.replaceAll('\n',' ');
        //system.debug('orderBody2: ' +  stringJSONdebug);
        
        
        ///REQUEST
        try 
        {
            //Request
            request.setHeader('Authorization', authorizationHeader);
            request.setMEthod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setEndpoint(endpoint);
            request.setBody(Accents.removeDiacritics(orderBody));
            //DEBUG REQUEST
            system.debug('Request: '+ request);
            system.debug('Request Body: '+ request.getBody());

            ////TEST////
            //Response and LOG
            // IntegrationLogUtility.includeHttpRequest(log, request);
            response = http.send(request);
            // IntegrationLogUtility.includeHttpResponse(log, response);
            responseBody = response.getBody();

            ///TEST///
            /*response.setHeader('Content-Type', 'application/json');
            response.setBody('')
			response.setStatusCode(200);
            responseBody = response.getBody();*/

            //DEBUG RESPONSE
            System.debug('Response StatusCode: '+ response.getstatusCode());
            System.debug('Response Body: '+ response.getBody());
            //System.debug('Response Body2: ' + JSON.serialize(response.getBody().replaceAll('\\n','<br/>')));
            actualOrder.Response_Comercia__c = response.getBody();
            
        } 
        catch (System.Exception e) 
        {
            system.debug('catch');
            System.debug('ERROR GeneralAPI:' + e.getTypeName() + ' - ' + e.getMessage() + ' - ' + e.getStackTraceString() + ' - ' + responseBody);
            // log.Error_Message__c = e.getMessage();
            responseBody = e.getTypeName() + ' - ' + e.getMessage() + ' - ' + e.getStackTraceString() + ' - ' + responseBody;
            actualOrder.Response_Comercia__c = response.getBody();
            actualOrder.Error_Comerzzia__c= true;
        }

        //RESPONSE
        try
        {
            
            /*
            //  CREATE CLASS(OrderResponse) if not return 200
                OrderResponse.Response responseParser;
                responseParser = OrderResponse.parse(responseBody);
                system.debug(responseParser);

                // IF string is not blank 
                if(String.isNotBlank(responseParser.success))
                {
                    if(responseParser.success == true){
                        result = 1;
                    }
                }
            */
            
            if(response.getstatusCode()==200)
            {
                actualOrder.Response_Comercia__c = response.getBody();
                system.debug('200');
                Object fieldList = (Object)JSON.deserializeUntyped(response.getBody()); 
                Map<String,Object> data = (Map<String,Object>)fieldList;
                Map<String, Object> productMap = (Map<String, Object>)data.get('Respuesta');
                Boolean successUpdate = (Boolean) productMap.get('success');
                if(successUpdate){
                    result = 1;
                    actualOrder.Error_Comerzzia__c= false;
                }else{
                    actualOrder.Error_Comerzzia__c= true;
                }

            }
            else{
                system.debug(response.getstatusCode());
                actualOrder.Error_Comerzzia__c= true;
                actualOrder.Response_Comercia__c = response.getBody();
            }
        }
        catch(System.Exception e)
        {
            system.debug('catch');
            actualOrder.Error_Comerzzia__c= true;
            actualOrder.Response_Comercia__c = response.getBody();
            // log.Error_Message__c = e.getMessage();
            // IntegrationLogUtility.publishLog(log);
        }


        //Publish LOG
        // IntegrationLogUtility.publishLog(log);
        
        return result;
    }
    
    private static String outsourceGenJSON(Order ord,List<Order_Line_Item__c> listOrdIt, List<Shipment__c> listShip, List<Payment_Information__c> listPay) 
    {
        
        Map<Decimal,Decimal> sumTax = new Map<Decimal,Decimal>();
        
        // Create a JSONGenerator object.
        JSONGenerator gen = JSON.createGenerator(true);        
        // Write data to the JSON string.
        gen.writeStartObject();
        
        // Create a JSONGenerator object OrderItem.
        JSONGenerator genOrderItem = JSON.createGenerator(true);        
        // Write data to the JSON string.
        genOrderItem.writeStartObject();
        
        
        //ORDER
        //ID
        if(ord.ID!=null)
            gen.writeStringField('IdOrden', ord.ID);
        else
            gen.writeStringField('IdOrden', '');
        
        if(ord.Original_Number__c!=null){
            gen.writeStringField('uid__pedido__origen', ord.Original_Number__c);
        }
        // // else
        // //     gen.writeStringField('uid__pedido__origen', '');
        
        //OrderNumberCommerceCloud
        if(ord.SFCC_Order_Number__c != null)
            gen.writeStringField('OrderNumberCommerceCloud', ord.SFCC_Order_Number__c);
        else
            gen.writeStringField('OrderNumberCommerceCloud', '');
        
        //AccountId
        if(ord.AccountId!=null && !ord.Account.Invitado__c)
            gen.writeStringField('AccountId', ord.AccountId);
        else
            gen.writeStringField('AccountId', '');
        
        //OrderStartDate
        // if(ord.EffectiveDate != null)
        // {
        //     // gen.writeStringField('OrderStartDate', string.valueOf(ord.EffectiveDate));
        //     String inpputString = '2021-02-04T09:16:49.000Z';
        //     DateTime resultDateTime = DateTime.ValueofGmt(inpputString.replace('T', ' '));
        //     String formatted = resultDateTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss');

        //     System.Debug('resultDateTime>> '+formatted);
        //     gen.writeStringField('OrderStartDate', formatted);
        // }
        // else
        //     gen.writeStringField('OrderStartDate', '');
        
        if(ord.Date_Creation_Comerce__c != null)
        {
            // gen.writeStringField('OrderStartDate', string.valueOf(ord.EffectiveDate));
            DateTime resultDateTime = ord.Date_Creation_Comerce__c;
            String formatted = resultDateTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss');

            System.Debug('resultDateTime>> '+formatted);
            gen.writeStringField('OrderStartDate', formatted);
        }
        else{
            DateTime resultDateTime = ord.CreatedDate;
            String formatted = resultDateTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss');
            gen.writeStringField('OrderStartDate', formatted);
        }
        //OrderTotalBase
        if(ord.SFCC_Order_Total__c !=null && ord.Merchandize_total_tax__c !=null && ord.Tax_total__c !=null)
        {
        //   Decimal temp = ord.SFCC_Order_Total__c - ord.Merchandize_total_tax__c - ord.Tax_total__c;
          Decimal temp = ord.SFCC_Order_Total__c - ord.Tax_total__c;
            gen.writeStringField('OrderTotalBase', string.valueOf(temp));  
        }
        else
            gen.writeStringField('OrderTotalBase', '0');
        //OrderTotalTax
        if(ord.Tax_total__c!=null)
        {
            gen.writeStringField('OrderTotalTax', string.valueOf(ord.Tax_total__c));
        }
        else
            gen.writeStringField('OrderTotalTax', '0');
        
        //OrderTotal
        if(ord.SFCC_Order_Total__c != null)
            gen.writeStringField('OrderTotal', string.valueOf(ord.SFCC_Order_Total__c));
        else
            gen.writeStringField('OrderTotal', '');
        
        //NumTarjetaFidelizado
        if(Ord.Account.Numero_de_tarjeta_CZZ__c != null && !ord.Account.Invitado__c)
            gen.writeStringField('NumTarjetaFidelizado',Ord.Account.Numero_de_tarjeta_CZZ__c);
        else
            gen.writeStringField('NumTarjetaFidelizado', '');
        
            //Status
        if(Ord.Status != null)
            gen.writeStringField('Status',Ord.Status);
        else
            gen.writeStringField('Status', '');
        
            //EMO_SITE
        if(Ord.SFCC_Order_Number__c.left(4)==Label.OrderNumberPeninsula){
            gen.writeStringField('EMO_SITE',Label.SiteIdPeninsula);
        }else if(Ord.SFCC_Order_Number__c.left(4)==Label.OrderNumberCanarias){
            gen.writeStringField('EMO_SITE', Label.SiteIdCanarias);
        }else{
            gen.writeStringField('EMO_SITE', '');
        }
        // Reason__c
        if(ord.Reason__c != null){
            gen.writeStringField('Reason__c', ord.Reason__c);            
        }else{
            gen.writeStringField('Reason__c', '');
        }
        // Notes__c
        if(ord.Notes__c != null){
            gen.writeStringField('Notes__c', ord.Notes__c);            
        }else{
            gen.writeStringField('Notes__c', '');
        }
        // Decision__c
        if(ord.Decision__c != null){
            gen.writeStringField('Decision__c', ord.Decision__c);            
        }else{
            gen.writeStringField('Decision__c', '');
        }
        // Payout__c
        if(ord.Payout__c ){
            gen.writeStringField('Payout__c', String.valueOf(ord.Payout__c));            
        }else{
            gen.writeStringField('Payout__c', String.valueOf(ord.Payout__c));
        }
        // PickUpOnStoreCode__c
        if(ord.PickUpOnStoreCode__c != null){
            gen.writeStringField('PickUpOnStoreCode__c', ord.PickUpOnStoreCode__c);            
        }else{
            gen.writeStringField('PickUpOnStoreCode__c', '');
        }
        // refundStatus__c
        if(ord.refundStatus__c != null){
            gen.writeStringField('refundStatus__c', ord.refundStatus__c);            
        }else{
            gen.writeStringField('refundStatus__c', '');
        }
        // resultLog__c
        if(ord.resultLog__c != null){
            gen.writeStringField('resultLog__c', ord.resultLog__c);            
        }else{
            gen.writeStringField('resultLog__c', '');
        }
        // shippingCost__c
        if(ord.shippingCost__c){
            gen.writeStringField('shippingCost__c', String.valueOf(ord.shippingCost__c));            
        }else{
            gen.writeStringField('shippingCost__c', String.valueOf(ord.shippingCost__c));
        }
        //ORDERITEM
        gen.writeFieldName('product_items');
        gen.writeStartArray();
        if(listOrdIt!=null){
            For(Order_Line_Item__c actOI : listOrdIt){
                 
                /////////////////////
                //CALC SUMARY TAXES//
                //////////////////////
                Decimal totalTax = 0;
                 if(sumTax.containsKey(actOI.Tax_rate__c)){
                    totalTax = sumTax.get(actOI.Tax_rate__c);
                    
                    totalTax = totalTax + actOI.Tax__c;
                    
                    sumTax.put(actOI.Tax_rate__c,totalTax);
                }
                else if(!sumTax.containsKey(actOI.Tax_rate__c)){
                    totalTax = actOI.Tax__c;
                    sumTax.put(actOI.Tax_rate__c,totalTax);
                }
                
                
                //Start actual
                gen.writeStartObject();
                
                //Price
                if(actOI.Base_price__c != null)
                {
                    gen.writeStringField('Price',string.valueOf(actOI.Base_price__c));
                }
                else
                    gen.writeStringField('Price', ''); 
                
                //PriceAfterItemDiscount
                if(actOI.Price_After_Item_Discount__c != null)
                   {
                    gen.writeStringField('PriceAfterItemDiscount',string.valueOf(actOI.Price_After_Item_Discount__c));
                }
                else
                    gen.writeStringField('PriceAfterItemDiscount', ''); 
                
                //PriceAfterOrderDiscount
                if(actOI.Price_After_Order_Discount__c != null)
                   {
                    gen.writeStringField('PriceAfterOrderDiscount',string.valueOf(actOI.Price_After_Order_Discount__c));
                }
                else
                    gen.writeStringField('PriceAfterOrderDiscount', ''); 
                
                //ProductId
                if(actOI.Product_Id__c != null)
                   {
                    gen.writeStringField('ProductId',actOI.Product_Id__c);
                }
                else
                    gen.writeStringField('ProductId', ''); 
                
                //ProductName
                if(actOI.Product_Name__c != null)
                   {
                    gen.writeStringField('ProductName',actOI.Product_Name__c);
                }
                else
                    gen.writeStringField('ProductName', ''); 
                
                //Quantity
                if(actOI.Quantity__c != null)
                   {
                    gen.writeStringField('Quantity',string.valueOf(actOI.Quantity__c));
                }
                else
                    gen.writeStringField('Quantity', ''); 
                
                //Tax
                if(actOI.Adjusted_tax__c!= null)
                // if(actOI.Tax__c != null)
                   {
                    if(actOI.Quantity__c>1 && actOI.Adjusted_tax__c!= null){
                        Decimal toroundTax = actOI.Adjusted_tax__c/actOI.Quantity__c;
                        gen.writeStringField('Tax',string.valueOf(toroundTax.setScale(2,RoundingMode.FLOOR)));                    
                    }else{
                        gen.writeStringField('Tax',string.valueOf(actOI.Adjusted_tax__c));
                            // gen.writeStringField('Tax',string.valueOf(actOI.Tax__c));
                    }
                }
                else
                    gen.writeStringField('Tax', '0'); 
                
                //Tax_rate
                if(actOI.Tax_rate__c != null)
                   {
                    gen.writeStringField('Tax_rate',string.valueOf(actOI.Tax_rate__c));
                }
                else
                    gen.writeStringField('Tax_rate', ''); 
                
                //UnitPrice
                if(actOI.Unit_Price__c != null)
                   {
                    if(actOI.Quantity__c>1){
                        Decimal toroundUnit = actOI.Price_After_Item_Discount__c/actOI.Quantity__c;
                        // gen.writeStringField('UnitPrice',string.valueOf(actOI.Unit_Price__c));
                        gen.writeStringField('UnitPrice',string.valueOf(toroundUnit.setScale(2,RoundingMode.FLOOR)));
                    }else{
                        gen.writeStringField('UnitPrice',string.valueOf(actOI.Price_After_Item_Discount__c));

                    }
                }
                else
                    gen.writeStringField('UnitPrice', ''); 
                
                //VariantInfo
                if(actOI.Variant_Info__c != null)
                   {
                    gen.writeStringField('VariantInfo',actOI.Variant_Info__c);
                }
                else
                    gen.writeStringField('VariantInfo', ''); 
                
                //END actual
                gen.writeEndObject();
            }
        }
        
        gen.writeEndArray();
        
        //gen.writeObjectField('product_items', idSAPDireccion);
        system.debug('sumTax:: '+ sumTax);
        
        
         
		//Summary Taxes
		gen.writeFieldName('summary_taxes');
        gen.writeStartArray();
        
        For(Decimal taxRate : sumTax.keySet()){
			//Start actual
            gen.writeStartObject();
            
        	//Shipment_No
            // if(sumTax.get(taxRate) != null)
            if(sumTax.get(taxRate) != null && ord.Tax_total__c!=null)
            {
                // gen.writeStringField('AmountTax',string.valueof(sumTax.get(taxRate)));
                gen.writeStringField('AmountTax',string.valueof(ord.Tax_total__c));
            }
        	else
            	gen.writeStringField('AmountTax', '0.0');
            
            //tax_rate
            if(taxRate != null)
            {
                gen.writeStringField('tax_rate',string.valueof(taxRate));
            }
        	else
            	gen.writeStringField('tax_rate', '');

			//END actual
            gen.writeEndObject();
        }
		
        gen.writeEndArray();

        //Shipment
        gen.writeFieldName('shipments');
        gen.writeStartArray();
        if(listShip!=null){

            For(Shipment__c actShip : listShip){
                //Start actual
                gen.writeStartObject();
                
                //Shipment_No
                if(actShip.Shipment_No__c != null)
                {
                    gen.writeStringField('Shipment_No',actShip.Shipment_No__c);
                }
                else
                    gen.writeStringField('Shipment_No', '');
                
                //Shipping_Address
                if(actShip.Shipping_Address__c != null)
                {
                    gen.writeStringField('Shipping_Address',actShip.Shipping_Address__c);
                }
                else
                    gen.writeStringField('Shipping_Address', '');
                
                //Shipping_Method
                if(actShip.Shipping_Method__c != null)
                {
                    gen.writeStringField('Shipping_Method',actShip.Shipping_Method__c);
                }
                else
                    gen.writeStringField('Shipping_Method', '');
                
                //Shipping_Total_SinTax
                if(actShip.Shipment_Total__c != null && actShip.Shipping_Total_Tax__c != null)
                {
                    Decimal totalSin = actShip.Shipment_Total__c - actShip.Shipping_Total_Tax__c;
                    gen.writeStringField('Shipping_Total_SinTax',string.valueOf(totalSin));
                }
                else
                    gen.writeStringField('Shipping_Total_SinTax', '0');
                
                //Shipping_Total_Tax
                if(actShip.Shipping_Total_Tax__c	 != null)
                {
                    gen.writeStringField('Shipping_Total_Tax',string.valueof(actShip.Shipping_Total_Tax__c));
                }
                else
                    gen.writeStringField('Shipping_Total_Tax', '0');
                
                //Shipping_Total
                if(actShip.Order__r.SFCC_Order_Total__c != null && actShip.Order__r.Producto_total__c!=null)
                // if(actShip.Shipment_Total__c	 != null)
                {
                    Decimal toroundShipmentTotal =   actShip.Order__r.SFCC_Order_Total__c - actShip.Order__r.Producto_total__c;                 
                    // gen.writeStringField('Shipping_Total',string.valueof(actShip.Shipping_Total_Tax__c));
                    gen.writeStringField('Shipping_Total',string.valueof(toroundShipmentTotal.setScale(2,RoundingMode.FLOOR)));
                }
                else
                    gen.writeStringField('Shipping_Total', '0');
                
                //END actual
                gen.writeEndObject();
                
            }
        }
        gen.writeEndArray();
        
        //Payment
        gen.writeFieldName('payment_methods');
        gen.writeStartArray();

        if(listPay!=null){
            For(Payment_Information__c actPay : listPay){
                
                //Start actual
                gen.writeStartObject();
                
                //Amount_Charged_to_Card
                if(actPay.Amount_Charged_to_Card__c != null)
                {
                    gen.writeStringField('Amount_Charged_to_Card',string.valueOf(actPay.Amount_Charged_to_Card__c));
                }
                else
                    gen.writeStringField('Amount_Charged_to_Card', '');
                
                //Payment_Method
                  if(actPay.Payment_Method__c != null)
                {
                    gen.writeStringField('Payment_Method',actPay.Payment_Method__c);
                }
                else
                    gen.writeStringField('Payment_Method', '');
                
                //END actual
                gen.writeEndObject();
            }

        }

        
        gen.writeEndArray();
        
        
        ///////////////////////////
        /////Get the JSON string.//
        ///////////////////////////
        String getString = gen.getAsString();
        System.debug('JSON created: '+ getString);
        return getString;
        
    }  
}