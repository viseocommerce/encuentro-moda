public class ContactHelper {
	private List<Contact> oldList; 
    private List<Contact> newList; 
    private Map<id, Contact> oldMap;
    private Map<id, Contact> newMap;
    
    public ContactHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Contact>) Trigger.oldMap;
        this.newMap = (Map<id, Contact>) Trigger.newMap;
    }
}