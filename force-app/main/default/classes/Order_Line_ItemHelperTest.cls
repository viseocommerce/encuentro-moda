@isTest
public class Order_Line_ItemHelperTest {

    @testSetup
    static void setup() {
        Account acc = new Account( FirstName = 'fname', LastName = 'lname', personEmail ='test@hotmail.com', SFCC_Customer_Number__pc = '12345', Contact_Status__pc  = 'Created');
        Insert acc; 
        
        SCCTestDataFactory.createContact(new Map<Integer, String>{1=>'fname', 2=>'fname', 3=>'fname'}, new Map<Integer, String>{1=>'lname', 2=>'lname', 3=>'lname'}, new Map<Integer, String>{1=>'test@hotmail.com', 2=>'test@yahoo.com', 3=>'test@salesforce.com'});
        // SCCTestDataFactory.createAccount(new Map<Integer, String>{1=>'fname', 2=>'fname', 3=>'fname'}, new Map<Integer, String>{1=>'lname', 2=>'lname', 3=>'lname'}, new Map<Integer, String>{1=>'test@hotmail.com', 2=>'test@yahoo.com', 3=>'test@salesforce.com'}, new Map<Integer, String>{1=>'12345', 2=>'23456', 3=>'34567'});
        SCCTestDataFactory.createOrder(5, [SELECT Id FROM Account LIMIT 1].Id, System.today(), 'Draft', '12345', [SELECT Id FROM Contact LIMIT 1].Id);
    }

    @isTest
    static void avoidDuplicate(){
        Order order = [SELECT Id, SFCC_Order_Number__c FROM Order LIMIT 1];
        Test.startTest();
        List<Order_Line_Item__c> listItems = new List<Order_Line_Item__c>();
        Order_Line_Item__c test1 = new Order_Line_Item__c();
        test1.Product_Id__c = '12345';
        test1.Order__c = order.id;
        listItems.add(test1);
        Order_Line_Item__c test2 = new Order_Line_Item__c();
        test2.Product_Id__c = '12345';
        test2.Order__c = order.id;
        listItems.add(test2);
        Insert listItems;
        Update test1;

        List<Shipment__c> Shipments = new List<Shipment__c>();
        Shipment__c ShipmentTest = new Shipment__c();
        ShipmentTest.Shipment_No__c = '12345';
        ShipmentTest.Order__c = order.id;
        Shipments.add(ShipmentTest);
        Shipment__c ShipmentTest2 = new Shipment__c();
        ShipmentTest2.Shipment_No__c = '12345';
        ShipmentTest2.Order__c = order.id;
        Shipments.add(ShipmentTest2);
        Insert Shipments;
        Update ShipmentTest;

        List<Payment_Information__c> payments = new List<Payment_Information__c>();
        Payment_Information__c paymentTest = new Payment_Information__c();
        paymentTest.Card_Number__c = '12345';
        paymentTest.Order__c = order.id;
        payments.add(paymentTest);
        Payment_Information__c paymentTest2 = new Payment_Information__c();
        paymentTest2.Card_Number__c = '12345';
        paymentTest2.Order__c = order.id;
        payments.add(paymentTest2);
        Insert payments;
        Update paymentTest;
        Test.stopTest();
    }
}