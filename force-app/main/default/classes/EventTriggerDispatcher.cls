public class EventTriggerDispatcher implements TriggerDispatcher{
    private EventHelper helper;
    public EventTriggerDispatcher() {
        helper = new EventHelper();
    }
    public void bulkBefore() {
        System.debug('Event (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Event (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Event (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Event (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Event (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Event (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Event (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Event (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Event (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Event (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.EventTriggerEnabled);
    }
}