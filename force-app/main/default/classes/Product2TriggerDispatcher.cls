public class Product2TriggerDispatcher implements TriggerDispatcher{
    private Product2Helper helper;
    public Product2TriggerDispatcher() {
        helper = new Product2Helper();
    }
    public void bulkBefore() {
        System.debug('Product (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Product (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Product (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Product (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Product (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Product (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Product (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Product (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Product (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Product (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.Product2TriggerEnabled);
    }
}