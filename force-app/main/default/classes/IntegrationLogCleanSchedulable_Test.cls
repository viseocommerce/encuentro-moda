/*
*   @brief  Test class for IntegrationLogCleanBatch
*   @details N/A
*   @author Viseo
*   @see IntegrationLogCleanBatch,IntegrationLogCleanSchedulable,IntegrationLogCleanSchedulable_Test

*   @version 1.0
*   @date 2019/11/20
*/
@isTest
public class IntegrationLogCleanSchedulable_Test {

    static testmethod void AccountShareScheTest(){
        
        test.startTest();
        
            String sch = '0 0 2 * * ?';
            String jobId2 = System.Schedule('IntegrationLogCleanSchedulable', sch, new IntegrationLogCleanSchedulable());    
        
        test.stopTest();
    }
    
}