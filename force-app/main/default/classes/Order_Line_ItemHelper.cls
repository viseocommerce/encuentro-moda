public class Order_Line_ItemHelper {
	private List<Order_Line_Item__c> oldList; 
    private List<Order_Line_Item__c> newList; 
    private Map<id, Order_Line_Item__c> oldMap;
    private Map<id, Order_Line_Item__c> newMap;
    
    public Order_Line_ItemHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Order_Line_Item__c>) Trigger.oldMap;
        this.newMap = (Map<id, Order_Line_Item__c>) Trigger.newMap;
    }

    public void avoidDuplicateOrders() {
        List<Order_Line_Item__c> itemsToDelete = new List<Order_Line_Item__c>();
        List<id> ordersIds = new List<id>();
        List<String> productIds = new List<String>();
        for(Order_Line_Item__c orderIt: this.newList) {
            ordersIds.add(orderIt.Order__c);
            productIds.add(orderIt.Product_Id__c);
        }
        List<Order_Line_Item__c> listItems =[SELECT id,Order__c,Product_Id__c FROM Order_Line_Item__c
                                                WHERE Order__c IN:ordersIds AND Product_Id__c IN:productIds
                                                ORDER BY Product_Id__c];

        String orderId ;
        String productId ;
        for (Order_Line_Item__c item : listItems) {
            if(item.Order__c == orderId && item.Product_Id__c == productId){
                itemsToDelete.add(item);
            }
            orderId = item.Order__c;
            productId = item.Product_Id__c;
        }
        System.debug('Items to delete :: '+ itemsToDelete);
        delete itemsToDelete;
    }
}