public class Order_Line_ItemTriggerDispatcher implements TriggerDispatcher{
    private Order_Line_ItemHelper helper;
    public Order_Line_ItemTriggerDispatcher() {
        helper = new Order_Line_ItemHelper();
    }
    public void bulkBefore() {
        System.debug('OrderItem (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('OrderItem (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('OrderItem (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('OrderItem (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('OrderItem (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('OrderItem (Before Delete)');
    }
    
    public void afterInsert() {
        helper.avoidDuplicateOrders();
        System.debug('OrderItem (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('OrderItem (After Update)');
    }
    
    public void afterDelete() {
        System.debug('OrderItem (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('OrderItem (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.OrderLineItemTriggerEnabled);
    }
}