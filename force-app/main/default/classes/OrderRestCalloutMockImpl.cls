@isTest
global class OrderRestCalloutMockImpl implements HttpCalloutMock{
    global HttpResponse respond(HttpRequest req){
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"Respuesta": {"success": true,"error": "Se ha introducido correctamente los datos de la factura:  UID_TICKET: 11111 - ID TICKET: 5770096"}}');
        res.setStatusCode(200);
        return res;
    } 
}