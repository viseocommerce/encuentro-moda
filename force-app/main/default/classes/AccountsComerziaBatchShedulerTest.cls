@isTest
public class AccountsComerziaBatchShedulerTest {
    
    @testSetup
    static void setup() {
       //Create Acc
       Account actualAcc = new account(
        FirstName = 'OOOO',
        LastName = 'AccTest',
        // Numero_de_tarjeta_CZZ__c = '1234',
        SFCC_Customer_Id__pc = 'abr8bZ3XsqR32xtxjFmmseSgIB',
        SFCC_Customer_Number__pc = '1231456',
        From_SFCC__pc = true,
        RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId()
        );
        insert actualAcc;
    }


    @isTest
    static void AccountsComerziaBatchShedulerTest() {        
        
        List<Account> acc = [SELECT id, Lastname, Apellido1_CZZ__c FROM Account LIMIT 1];
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new AccountsComerziaBatchShedulerTestMock());
            AccountsComerziaBatch b = new AccountsComerziaBatch(); 
            database.executebatch(b,1);
        Test.stopTest();

        //assertequals
        Account actualAccount = [SELECT id,Id_de_cliente_Comerzzia_CZZ__c,Invitado__c FROM Account];
        System.debug('Invitado :: '+actualAccount.Invitado__c);
        system.assertequals('10299',actualAccount.Id_de_cliente_Comerzzia_CZZ__c);

    }
    @isTest
    static void shedulerTest() {        
        Test.startTest();
        AccountsComerziaBatchSheduler myClass = new AccountsComerziaBatchSheduler();   
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, myClass);
        Test.stopTest();
   }

}