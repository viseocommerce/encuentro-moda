public class LiveChatTranscriptHelper {
    private List<LiveChatTranscript> oldList; 
    private List<LiveChatTranscript> newList; 
    private Map<id, LiveChatTranscript> oldMap;
    private Map<id, LiveChatTranscript> newMap;
     
    public LiveChatTranscriptHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, LiveChatTranscript>) Trigger.oldMap;
        this.newMap = (Map<id, LiveChatTranscript>) Trigger.newMap;
    }
    
	public void testHelper() {
        System.debug('TEST LiveChatTranscript');
    }
}