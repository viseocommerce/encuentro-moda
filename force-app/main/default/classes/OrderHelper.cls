public class OrderHelper {
	private List<Order> oldList; 
    private List<Order> newList; 
    private Map<id, Order> oldMap;
    private Map<id, Order> newMap;
    
    public OrderHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Order>) Trigger.oldMap;
        this.newMap = (Map<id, Order>) Trigger.newMap;
    }
}