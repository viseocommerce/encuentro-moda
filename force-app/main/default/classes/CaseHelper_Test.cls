@IStEST
public class CaseHelper_Test {
    static testMethod void myTestMethod(){
        
        
        //Folder
        Folder lstFolder = [Select Id From Folder limit 1];
        
        //Email Template
        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'name';
        validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
        validEmailTemplate.TemplateType = 'text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        
        insert validEmailTemplate;
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            //ACC
            Account accTest = new Account(
                FirstName = 'Mel',
                LastName = 'Roro',
                personemail = 'test@test.com'
            );
            //insert accTest;
            //Contact
            Contact conTest = new contact(
                FirstName = 'Mel',
                LastName = 'Roro',
                email = 'test@test.com'
            );
            insert conTest;
            
            
            
            //Case
            Case caseTest = new Case(
                //AccountId = accTest.ID,
                ContactId = conTest.ID,
                Description = 'Desc',
                Origin = 'Email',
                Type = 'Tienda fisica',
                Subtipo__c = 'Tienda - Disponibilidad',
                C_digo_de_tienda__c = 'E118'
            );
            
            insert caseTest;
        }  
        
        
    }
}