public class AccountHelper {
    private List<Account> oldList; 
    private List<Account> newList; 
    private Map<id, Account> oldMap;
    private Map<id, Account> newMap;
     
    public AccountHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Account>) Trigger.oldMap;
        this.newMap = (Map<id, Account>) Trigger.newMap;
    }
    
	public void setAnualIncome_BeforeInsert() {
        for(Account acc: this.newList) {
            acc.NumberOfEmployees = Integer.valueOf(Math.random() * 10);
        }
    }
	public void setFields_BeforeInsert() {
        for(Account acc: this.newList) {
            if(this.oldMap!=null){
                if(acc.PersonEmail!=this.oldMap.get(acc.Id).PersonEmail){
                    acc.EmailComerce__c=acc.PersonEmail;
                }else if(acc.EmailComerce__c!=this.oldMap.get(acc.Id).EmailComerce__c){
                    acc.PersonEmail=acc.EmailComerce__c;
                }
                if(acc.Nombre_CZZ__c!=this.oldMap.get(acc.Id).Nombre_CZZ__c || acc.Apellido1_CZZ__c!=this.oldMap.get(acc.Id).Apellido1_CZZ__c){
                    acc.FirstName =acc.Nombre_CZZ__c;
                    acc.LastName =acc.Apellido1_CZZ__c;
                }else if(acc.FirstName!=this.oldMap.get(acc.Id).FirstName || acc.LastName!=this.oldMap.get(acc.Id).LastName){
                    acc.Nombre_CZZ__c =acc.FirstName;
                    acc.Apellido1_CZZ__c =acc.LastName;
                }
                if(acc.Movil_CZZ__c!=this.oldMap.get(acc.Id).Movil_CZZ__c ){
                    acc.Phone =acc.Movil_CZZ__c;
                }else if(acc.Phone!=this.oldMap.get(acc.Id).Phone){
                    acc.Movil_CZZ__c =acc.Phone;
                }
                if(acc.Id_de_cliente_Comerzzia_CZZ__c!=null && acc.Ultima_modificaci_n_comerzzia__c==null){
                    acc.Ultima_modificaci_n_comerzzia__c= Datetime.now();
                }
              
                //Tratamiento de datos UPDATE//////////////////////////////////////////////
                if(acc.Ac_pol_tratamiento_de_datos_CZZ__c!=this.oldMap.get(acc.Id).Ac_pol_tratamiento_de_datos_CZZ__c){
                    acc.Fe_trat_datos_CZZ__c = Date.today();
                    if(acc.Updated_Comerzzia_CZZ__c){
                        acc.Sistema_de_alta_Tratamiento_datos__c = 'Comerzzia';                        
                    }else{
                        acc.Sistema_de_alta_Tratamiento_datos__c = 'SFCC';
                    }
                }
                if(acc.Acepta_notificaciones_comerciales_CZZ__c!=this.oldMap.get(acc.Id).Acepta_notificaciones_comerciales_CZZ__c){
                    acc.Fe_acep_notifi_com_CZZ__c = Date.today();
                    if(acc.Updated_Comerzzia_CZZ__c){
                        acc.Sistema_de_alta_Notificaciones_Comercial__c = 'Comerzzia';                        
                    }else{
                        acc.Sistema_de_alta_Notificaciones_Comercial__c = 'SFCC';
                    }
                }
                if(acc.Fidelizado_Registrado__c!=this.oldMap.get(acc.Id).Fidelizado_Registrado__c){
                    acc.Fecha_registro_fidelizado_Registrado__c = Date.today();
                    if(acc.Updated_Comerzzia_CZZ__c){
                        acc.Sistema_de_alta_Registrado_Fidelizado__c = 'Comerzzia';                        
                    }else{
                        acc.Sistema_de_alta_Registrado_Fidelizado__c = 'SFCC';
                    }
                }                
                
                //FIN Tratamiento de datos UPDATE/////////////////////////////////////////
                if(acc.Updated_Comerzzia_CZZ__c){
                    acc.Ultima_modificaci_n_comerzzia__c=Datetime.now();
                    acc.Updated_Comerzzia_CZZ__c=false;
                }                   
            }else{
                if(acc.PersonEmail!=null && acc.EmailComerce__c==null){
                    acc.EmailComerce__c=acc.PersonEmail;
                }else if(acc.EmailComerce__c!=null && acc.PersonEmail==null){
                    acc.PersonEmail=acc.EmailComerce__c;
                }
                if(acc.LastName==null && (acc.Apellido1_CZZ__c!=null && acc.Nombre_CZZ__c !=null)){
                    acc.LastName =acc.Apellido1_CZZ__c;
                    acc.FirstName =acc.Nombre_CZZ__c;
                    acc.PersonEmail =acc.EmailComerce__c;
                }
                if(acc.Movil_CZZ__c!=null){
                    acc.Phone =acc.Movil_CZZ__c;
                }
                if(acc.Id_de_cliente_Comerzzia_CZZ__c!=null && acc.Ultima_modificaci_n_comerzzia__c==null){
                    acc.Ultima_modificaci_n_comerzzia__c= Datetime.now();
                    acc.Updated_Comerzzia_CZZ__c=false;
                }
                //Tratamiento de datos INSERT//////////////////////////////////////////////
                if(acc.Ac_pol_tratamiento_de_datos_CZZ__c){
                    acc.Fe_trat_datos_CZZ__c = Date.today();
                    if(acc.From_SFCC__pc){
                        acc.Sistema_de_alta_Tratamiento_datos__c = 'SFCC';
                    }else{
                        acc.Sistema_de_alta_Tratamiento_datos__c = 'Comerzzia';
                    }
                }
                if(acc.Invitado__c){
                    acc.Sistema_de_alta_Invitado__c = 'SFCC';
                    acc.Fecha_alta_invitado__c = Date.today();
                }
                if(acc.Acepta_notificaciones_comerciales_CZZ__c){
                    acc.Fe_acep_notifi_com_CZZ__c = Date.today();
                    if(acc.From_SFCC__pc){
                        acc.Sistema_de_alta_Notificaciones_Comercial__c = 'SFCC';
                    }else{
                        acc.Sistema_de_alta_Notificaciones_Comercial__c = 'Comerzzia';
                    }
                }
                if(acc.Fidelizado_Registrado__c){
                    acc.Fecha_registro_fidelizado_Registrado__c =Date.today();
                    if(acc.From_SFCC__pc){
                        acc.Sistema_de_alta_Registrado_Fidelizado__c = 'SFCC';
                    }else{
                        acc.Sistema_de_alta_Registrado_Fidelizado__c = 'Comerzzia';
                    }
                }
                
                
                //FIN Tratamiento de datos INSERT/////////////////////////////////////////                
            }
        }
    }
	public void avoidDuplicateEmails_BeforeInsert() {
        List<String> emails =new List<String>();
        for(Account acc: this.newList) {
            if(acc.PersonEmail!=null){
                emails.add(acc.PersonEmail);
            }
        }
        Map<id,Account> accounts=new Map<id,Account>([SELECT id,PersonEmail FROM Account WHERE PersonEmail IN:emails]);
        System.debug('Account SIZE :: '+accounts.size());
        if(accounts.size()>0){
            for(Account acc: this.newList) {
                if(acc.PersonEmail==accounts.values()[0].PersonEmail){
                    acc.addError('The email have to be unique');
                }
            }
        }
    }

    public void updateAddress_BeforeUpdate() {
        if(this.oldMap!=null){
            Map <id,Account> addressClients = new Map <id,Account>();
            for(Account acc: this.newList) {
                if((acc.Tipo_de_via_CZZ__c!=this.oldMap.get(acc.Id).Tipo_de_via_CZZ__c || acc.Nombre_de_via_CZZ__c!=this.oldMap.get(acc.Id).Nombre_de_via_CZZ__c ||
                acc.Numero_CZZ__c!=this.oldMap.get(acc.Id).Numero_CZZ__c || acc.Complemento_CZZ__c!=this.oldMap.get(acc.Id).Complemento_CZZ__c ||
                acc.Codigo_postal_CZZ__c!=this.oldMap.get(acc.Id).Codigo_postal_CZZ__c || acc.Poblacion_CZZ__c!=this.oldMap.get(acc.Id).Poblacion_CZZ__c ||
                acc.Localidad_CZZ__c!=this.oldMap.get(acc.Id).Localidad_CZZ__c || acc.Provincia_CZZ__c!=this.oldMap.get(acc.Id).Provincia_CZZ__c ||
                acc.Pais_CZZ__c!=this.oldMap.get(acc.Id).Pais_CZZ__c) && acc.update_by_address__c== false){
                // acc.Pais_CZZ__c!=this.oldMap.get(acc.Id).Pais_CZZ__c) && acc.update_by_address__c==this.oldMap.get(acc.Id).update_by_address__c){
                    addressClients.put(acc.id, acc);
                }else{
                    acc.update_by_address__c = false;
                }                
            }
            List<Address__c> adressesUpdate =[SELECT id,Account__c,Address_Line_1__c FROM Address__c WHERE Account__c IN:addressClients.keySet() AND Sync_Address__c= true];
            if(adressesUpdate.size()>0){
                for(Address__c address:adressesUpdate){
                    address.Address_Line_1__c =addressClients.get(address.Account__c).Nombre_de_via_CZZ__c;
                    address.Address_Line_2__c =addressClients.get(address.Account__c).Complemento_CZZ__c;
                    address.City__c =addressClients.get(address.Account__c).Poblacion_CZZ__c;
                    address.Postal_Code__c =addressClients.get(address.Account__c).Codigo_postal_CZZ__c;
                    address.state_Name__c =addressClients.get(address.Account__c).Provincia_CZZ__c;
                    address.Country__c =addressClients.get(address.Account__c).Pais_CZZ__c;
                    address.update_by_account__c = true;
                }
                update adressesUpdate;
            }
        }
    }
}