public class Pricebook2Helper {
	private List<Pricebook2> oldList; 
    private List<Pricebook2> newList; 
    private Map<id, Pricebook2> oldMap;
    private Map<id, Pricebook2> newMap;
    
    public Pricebook2Helper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Pricebook2>) Trigger.oldMap;
        this.newMap = (Map<id, Pricebook2>) Trigger.newMap;
    }
}