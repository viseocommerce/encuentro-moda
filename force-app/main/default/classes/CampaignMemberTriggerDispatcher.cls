public class CampaignMemberTriggerDispatcher implements TriggerDispatcher {
	private CampaignMemberHelper helper;
    public CampaignMemberTriggerDispatcher() {
        helper = new CampaignMemberHelper();
    }
    public void bulkBefore() {
        System.debug('CampaignMember (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('CampaignMember (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('CampaignMember (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('CampaignMember (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('CampaignMember (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('CampaignMember (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('CampaignMember (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('CampaignMember (After Update)');
    }
    
    public void afterDelete() {
        System.debug('CampaignMember (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('CampaignMember (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.CampaignMemberTriggerEnabled);
    }
}