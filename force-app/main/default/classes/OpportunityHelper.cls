public class OpportunityHelper {
    private List<Opportunity> oldList; 
    private List<Opportunity> newList; 
    private Map<id, Opportunity> oldMap;
    private Map<id, Opportunity> newMap;
    
    public OpportunityHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Opportunity>) Trigger.oldMap;
        this.newMap = (Map<id, Opportunity>) Trigger.newMap;
    }
}