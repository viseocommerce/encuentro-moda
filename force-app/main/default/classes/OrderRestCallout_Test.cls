@isTest
public class OrderRestCallout_Test {
    @isTest
    static void OrderRestCallout_Test() {

        //Create Acc
        Account actualAcc = new account(
            FirstName = 'OOOO',
            LastName = 'AccTest',   
            Numero_de_tarjeta_CZZ__c = '1234',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId()
        );
        Insert actualAcc;

        //CREATE ORDER
        Order actualOrder = new Order(
            SFCC_Order_Number__c = 'EOLM2100013234',
            AccountId = actualAcc.ID,
            EffectiveDate = system.today(),
            SFCC_Order_Total__c = 23.88,
            Merchandize_total_tax__c = 3.47,
            Tax_total__c = 4.15,
            Order_SCCSync_Status__c = 'Created',
            Status = 'Draft',            
            Date_Creation_Comerce__c = Date.today()
        );

        insert actualOrder;

        //OrderItem
        List<Order_Line_Item__c> listOrderItem = new List<Order_Line_Item__c>();
        //ORDERITEM1
        Order_Line_Item__c actOrdItem1 =  new Order_Line_Item__c(
            Order__c = actualOrder.ID,
            name = 'TestOrder',
            Base_price__c = null,
            Price_After_Item_Discount__c = 19.98,
            Price_After_Order_Discount__c = 19.98,
            Product_Id__c = '2023020150341M',
            Product_Name__c = 'CAMISETA ESPALDA DESCUBIERTA',
            Quantity__c = 2.0,
            Tax__c = 3.47,
            Tax_rate__c = 0.21,
            Unit_Price__c = 9.99,
            Variant_Info__c = 'Color:Verde Amazona ; Tamaño:M'
        );
        
        insert actOrdItem1;

        //ORDERITEM1
        Order_Line_Item__c actOrdItem2 =  new Order_Line_Item__c(
            Order__c = actualOrder.ID,
            Base_price__c = null,
            Price_After_Item_Discount__c = 19.98,
            Price_After_Order_Discount__c = 19.98,
            Product_Id__c = '2023020150341M',
            Product_Name__c = 'CAMISETA ESPALDA DESCUBIERTA',
            Quantity__c = 2.0,
            Tax__c = 3.47,
            Tax_rate__c = 0.21,
            Unit_Price__c = 9.99,
            Variant_Info__c = 'Color:Verde Amazona ; Tamaño:M',
            Adjusted_tax__c = 3.99
        );
        
        insert actOrdItem2;

        listOrderItem.add(actOrdItem1);
        listOrderItem.add(actOrdItem2);

        //SHIPMENT
        List<Shipment__c> listShipment = new  List<Shipment__c>();
        //SHIPMENT1
        Shipment__c actShip1 = new Shipment__c(
            Order__c = actualOrder.ID,
            Shipment_No__c = '00008001',
            Shipping_Address__c = 'Prueba, asdad, 11 11100',
            Shipping_Method__c ='Envío a domicilio',
            Shipment_Total__c = 23.88,
            Shipping_Total_Tax__c = 0.68
        );
        insert actShip1;

        //SHIPMENT1
        Shipment__c actShip2 = new Shipment__c(
            Order__c = actualOrder.ID,
            Shipment_No__c = '00008001',
            Shipping_Address__c = 'Prueba, asdad, 11 11100',
            Shipping_Method__c ='Envío a domicilio',
            Shipment_Total__c = 23.88,
            Shipping_Total_Tax__c = 0.68
        );
        insert actShip2;

        listShipment.add(actShip1);
        listShipment.add(actShip2);


        //PAYMENT
        List<Payment_Information__c> listPayment = new List<Payment_Information__c>();
        //PAYMENT1
        Payment_Information__c actualPay1 = new Payment_Information__c(
            Order__c = actualOrder.ID,
            Amount_Charged_to_Card__c = 23.88,
            Payment_Method__c = 'CREDIT_CARD'
        );

        insert actualPay1;

        //PAYMENT1
        Payment_Information__c actualPay2 = new Payment_Information__c(
            Order__c = actualOrder.ID,
            Amount_Charged_to_Card__c = 23.88,
            Payment_Method__c = 'CREDIT_CARD'
        );

        insert actualPay2;
        listPayment.add(actualPay1);
        listPayment.add(actualPay2);
        

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OrderRestCalloutMockImpl());
        List<Order> orders =[SELECT id,SFCC_Order_Number__c,AccountId,EffectiveDate,SFCC_Order_Total__c,Merchandize_total_tax__c,
                                    Tax_total__c,Order_SCCSync_Status__c,Status, CreatedDate,Date_Creation_Comerce__c,Account.Numero_de_tarjeta_CZZ__c,Account.Invitado__c,
                                    Reason__c,Notes__c,Decision__c,Payout__c,PickUpOnStoreCode__c,refundStatus__c,resultLog__c,shippingCost__c, Original_Number__c
                            FROM Order
                            WHERE SFCC_Order_Number__c = 'EOLM2100013234' LIMIT 1];
        Integer result = OrderRestCallout.callout(orders[0],listOrderItem,listShipment,listPayment);

        Test.stopTest();

        //Assert
        system.assertequals(1,result);
    }
}