/*
*   @brief  
*   @details N/A
*   @author Viseo
*   @see 

*   @version 1.0
*   @date 2020/03/31
*/
public class IntegrationLogCleanBatch implements Database.Batchable<SObject> {
    public IntegrationLogCleanBatch(){
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = IntegrationLogUtility.buildIntegrationLogExpirationQuery();
        System.debug([select Id,Module__c,CreatedDate from Integration_Log__c]);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        delete scope;
    }
    
    public void finish(Database.BatchableContext BC){
    }
}