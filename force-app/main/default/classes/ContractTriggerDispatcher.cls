public class ContractTriggerDispatcher implements TriggerDispatcher{
    private ContractHelper helper;
    public ContractTriggerDispatcher() {
        helper = new ContractHelper();
    }
    public void bulkBefore() {
        System.debug('Contract (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Contract (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Contract (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Contract (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Contract (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Contract (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Contract (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Contract (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Contract (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Contract (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.ContractTriggerEnabled);
    }
}