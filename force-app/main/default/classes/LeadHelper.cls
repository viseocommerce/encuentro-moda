public class LeadHelper {
	private List<Lead> oldList; 
    private List<Lead> newList; 
    private Map<id, Lead> oldMap;
    private Map<id, Lead> newMap;
    
    public LeadHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Lead>) Trigger.oldMap;
        this.newMap = (Map<id, Lead>) Trigger.newMap;
    }
}