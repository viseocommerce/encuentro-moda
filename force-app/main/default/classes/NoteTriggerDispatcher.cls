public class NoteTriggerDispatcher implements TriggerDispatcher{
    private NoteHelper helper;
    public NoteTriggerDispatcher() {
        helper = new NoteHelper();
    }
    public void bulkBefore() {
        System.debug('Note (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Note (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Note (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Note (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Note (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Note (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Note (After Insert)');
    }
    
    public void afterUpdate() {
        System.debug('Note (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Note (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Note (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.NoteTriggerEnabled);
    }
}