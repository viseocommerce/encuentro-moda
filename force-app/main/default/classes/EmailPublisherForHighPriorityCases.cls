global class EmailPublisherForHighPriorityCases implements QuickAction.QuickActionDefaultsHandler {
    // Empty constructor
    global EmailPublisherForHighPriorityCases() {
    }
    
    // The main interface method
    global void onInitDefaults(QuickAction.QuickActionDefaults[] defaults) {
    QuickAction.SendEmailQuickActionDefaults sendEmailDefaults = (QuickAction.SendEmailQuickActionDefaults)defaults.get(0);
    EmailMessage emailMessage = (EmailMessage)sendEmailDefaults.getTargetSObject(); 
    
    Case c = [SELECT CaseNumber, Type FROM Case WHERE Id=:sendEmailDefaults.getContextId()];    
    
    // If case type is “Problem,” insert the “First Response” email template
   
        sendEmailDefaults.setTemplateId('00X1l000000dlN6EAI');   // Set the template Id corresponding to First Response
        sendEmailDefaults.setInsertTemplateBody(false);   // para borrar el contenido del mail antes de insertar la plantilla
        sendEmailDefaults.setIgnoreTemplateSubject(false);
    }
}