global class OrderRestCalloutBatchSheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        OrderRestCalloutBatch job = new OrderRestCalloutBatch();
        Database.executeBatch(job,1);       
    }
}