global class AccountsComerziaBatch implements Database.Batchable<SObject>, Database.AllowsCallouts{
    
    // public AccountsComerziaBatch(){
    
    // }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {       
        String query;        
        query =      'SELECT id,Name,FirstName,LastName,Nombre_CZZ__c,Apellido1_CZZ__c,Apellido_2_CZZ__c,Tipo_de_via_CZZ__c,Nombre_de_via_CZZ__c,Complemento_CZZ__c,Numero_CZZ__c,Codigo_postal_CZZ__c,Localidad_CZZ__c,';
        query +=     'Poblacion_CZZ__c,Provincia_CZZ__c,Pais_CZZ__c,Tipo_de_documento_CZZ__c,N_documento_CZZ__c,Nacionalidad_CZZ__c,Fecha_de_nacimiento_CZZ__c,Sexo_CZZ__c,';
        query +=     'Estado_Civil_CZZ__c,Observaciones_CZZ__c,Activo_CZZ__c,Fecha_de_alta_Comerzzia_CZZ__c,Editable_Comerzzia_CZZ__c,Motivo_de_baja_Comerzzia_CZZ__c,Acepta_notificaciones_comerciales_CZZ__c,';
        query +=     'Id_de_cliente_Comerzzia_CZZ__c,Tienda_Captacion_CZZ__c,Id_Usuario_Alta_CZZ__c,Id_Usuario_Baja_CZZ__c,Numero_fidelizado_CZZ__c,Grupo_fidelizado_CZZ__c,SFCC_Customer_Id__pc,';
        query +=     'Ac_pol_tratamiento_de_datos_CZZ__c,Fe_trat_datos_CZZ__c,Movil_CZZ__c,Permitido_contacto_por_Mail_CZZ__c,Permitido_contacto_por_telefono_CZZ__c,Cod_Tipo_Contacto_CZZ__c,';
        query +=     'Valor_CZZ__c,Recibe_Notificaciones_CZZ__c,Recibe_Notificaciones_Comerciales_CZZ__c,Id_Tarjeta_CZZ__c,Numero_de_tarjeta_CZZ__c,Id_Cuenta_Tarjeta_CZZ__c,Fecha_Emision_CZZ__c,';
        query +=     'Fecha_Activacion_CZZ__c,Fecha_Ultimo_uso_CZZ__c,Fecha_baja_CZZ__c,Fecha_asignacin_fidelizado_CZZ__c,Cod_Tipo_Tarjeta_CZZ__c,Id_Clase_CZZ__c,Id_Objeto_CZZ__c,EmailComerce__c,SegmentoM__c';
        query +=    ' FROM Account';
        // query +=    ' WHERE id=\'0011l00000pPNcVAAW\'';
        query +=    ' WHERE (Id_de_cliente_Comerzzia_CZZ__c=null OR Id_de_cliente_Comerzzia_CZZ__c=\'0\' OR Modificar_en_Comerzzia__c=true OR Error_Comerzzia__c=true) AND (Created_Comerzzia_CZZ__c = true OR From_SFCC__pc = true) AND Invitado__c=false Order BY LastModifiedDate ASC';

        return Database.getQueryLocator(query);   

    }
    
    global void execute(Database.BatchableContext bc, List<Account> clientes) {

        for(Account cliente:clientes){
            AccountComerzia clientComerzia = new AccountComerzia();
            clientComerzia.DATOS = new AccountComerzia.AccountData();
            // clientComerzia.DATOS.Nombre                             = cliente.Nombre_CZZ__c !=null ? cliente.Nombre_CZZ__c : cliente.Name;          
            clientComerzia.DATOS.Nombre                             = cliente.Nombre_CZZ__c !=null ? cliente.Nombre_CZZ__c : cliente.FirstName;          
            clientComerzia.DATOS.Apellido1                          = cliente.Apellido1_CZZ__c !=null ? cliente.Apellido1_CZZ__c : cliente.LastName;               
            clientComerzia.DATOS.Apellido_2                         = cliente.Apellido_2_CZZ__c;               
            // clientComerzia.DATOS.Tipo_de_via                        = cliente.Tipo_de_via_CZZ__c;               
            clientComerzia.DATOS.Nombre_de_via                      = cliente.Nombre_de_via_CZZ__c;               
            // clientComerzia.DATOS.Numero                             = cliente.Numero_CZZ__c;               
            clientComerzia.DATOS.Complemento                        = cliente.Complemento_CZZ__c;               
            clientComerzia.DATOS.Codigo_postal                      = cliente.Codigo_postal_CZZ__c;               
            // clientComerzia.DATOS.Localidad                          = cliente.Localidad_CZZ__c;               
            clientComerzia.DATOS.Poblacion                          = cliente.Poblacion_CZZ__c;               
            clientComerzia.DATOS.Provincia                          = cliente.Provincia_CZZ__c;               
            clientComerzia.DATOS.Pais                               = cliente.Pais_CZZ__c;               
            clientComerzia.DATOS.Tipo_de_documento                  = cliente.Tipo_de_documento_CZZ__c;               
            clientComerzia.DATOS.N_documento                        = cliente.N_documento_CZZ__c;               
            clientComerzia.DATOS.Nacionalidad                       = cliente.Nacionalidad_CZZ__c;               
            clientComerzia.DATOS.Fecha_de_nacimiento                = String.valueOf(cliente.Fecha_de_nacimiento_CZZ__c);               
            clientComerzia.DATOS.Sexo                               = cliente.Sexo_CZZ__c;               
            clientComerzia.DATOS.Estado_Civil                       = cliente.Estado_Civil_CZZ__c;               
            clientComerzia.DATOS.Observaciones                      = cliente.Observaciones_CZZ__c;               
            clientComerzia.DATOS.Activo                             = String.valueOf(cliente.Activo_CZZ__c) ;               
            clientComerzia.DATOS.Fecha_de_alta_Comerzzia            = String.valueOf(cliente.Fecha_de_alta_Comerzzia_CZZ__c);                
            clientComerzia.DATOS.Editable_Comerzzia                  = String.valueOf(cliente.Editable_Comerzzia_CZZ__c);               
            clientComerzia.DATOS.Motivo_de_baja_Comerzzia           = cliente.Motivo_de_baja_Comerzzia_CZZ__c;               
            clientComerzia.DATOS.Id_de_cliente_Comerzzia            = cliente.Id_de_cliente_Comerzzia_CZZ__c;               
            clientComerzia.DATOS.Tienda_Captacion                   = cliente.Tienda_Captacion_CZZ__c;               
            clientComerzia.DATOS.Id_Usuario_Alta                    = String.valueOf((Integer)cliente.Id_Usuario_Alta_CZZ__c);               
            clientComerzia.DATOS.Id_Usuario_Baja                    = String.valueOf(cliente.Id_Usuario_Baja_CZZ__c);               
            clientComerzia.DATOS.Numero_fidelizado                  = cliente.Numero_fidelizado_CZZ__c;               
            clientComerzia.DATOS.Grupo_fidelizado                   = cliente.Grupo_fidelizado_CZZ__c;               
            // clientComerzia.DATOS.Ac_pol_tratamiento_de_datos        = String.valueOf(cliente.Ac_pol_tratamiento_de_datos_CZZ__c);               
            clientComerzia.DATOS.Ac_pol_tratamiento_de_datos        = String.valueOf(true);               
            clientComerzia.DATOS.Acepta_notificaciones_comerciales  = String.valueOf(cliente.Acepta_notificaciones_comerciales_CZZ__c);               
            clientComerzia.DATOS.Fe_trat_datos                      = String.valueOf(cliente.Fe_trat_datos_CZZ__c);               
            clientComerzia.DATOS.Movil                              = cliente.Movil_CZZ__c;               
            clientComerzia.DATOS.Permitido_contacto_por_Mail        = String.valueOf(cliente.Permitido_contacto_por_Mail_CZZ__c);               
            clientComerzia.DATOS.Permitido_contacto_por_telefono    = String.valueOf(cliente.Permitido_contacto_por_telefono_CZZ__c);               
            clientComerzia.DATOS.Cod_Tipo_Contacto                  = cliente.Cod_Tipo_Contacto_CZZ__c;               
            clientComerzia.DATOS.Valor                              = cliente.Valor_CZZ__c;               
            clientComerzia.DATOS.Recibe_Notificaciones              = String.valueOf(cliente.Recibe_Notificaciones_CZZ__c);               
            clientComerzia.DATOS.Recibe_Notificaciones_Comerciales  = String.valueOf(cliente.Recibe_Notificaciones_Comerciales_CZZ__c);               
            clientComerzia.DATOS.Id_Tarjeta                         = cliente.Id_Tarjeta_CZZ__c;               
            clientComerzia.DATOS.Numero_de_tarjeta                  = cliente.Numero_de_tarjeta_CZZ__c;               
            clientComerzia.DATOS.Id_Cuenta_Tarjeta                  = String.valueOf(cliente.Id_Cuenta_Tarjeta_CZZ__c);               
            clientComerzia.DATOS.Fecha_Emision                      = String.valueOf(cliente.Fecha_Emision_CZZ__c);               
            clientComerzia.DATOS.Fecha_Activacion                   = String.valueOf(cliente.Fecha_Activacion_CZZ__c);               
            clientComerzia.DATOS.Fecha_Ultimo_uso                   = String.valueOf(cliente.Fecha_Ultimo_uso_CZZ__c);               
            clientComerzia.DATOS.Fecha_baja                         = String.valueOf(cliente.Fecha_baja_CZZ__c);               
            clientComerzia.DATOS.Fecha_asignacin_fidelizado         = String.valueOf(cliente.Fecha_asignacin_fidelizado_CZZ__c);               
            clientComerzia.DATOS.Cod_Tipo_Tarjeta                   = cliente.Cod_Tipo_Tarjeta_CZZ__c;               
            clientComerzia.DATOS.Id_Clase                           = cliente.Id_Clase_CZZ__c;               
            clientComerzia.DATOS.Id_Objeto                          = cliente.Id_Objeto_CZZ__c;               
            clientComerzia.DATOS.IdSalesforce                       = cliente.Id;               
            clientComerzia.DATOS.EmailComerce						= cliente.EmailComerce__c;								
            clientComerzia.DATOS.Segmento						    = cliente.SegmentoM__c;								
            
            System.debug('Nombre: '+ clientComerzia.DATOS.Nombre );
            System.debug('Apellido1: '+ clientComerzia.DATOS.Apellido1 );
            System.debug('Salesforce Id: '+ clientComerzia.DATOS.IdSalesforce );
            System.debug('EmailComerce: '+ clientComerzia.DATOS.EmailComerce );
            System.debug('id Comerzia: '+ clientComerzia.DATOS.Id_de_cliente_Comerzzia );
            System.debug('id Comerzia account: '+ cliente.Id_de_cliente_Comerzzia_CZZ__c );
            String orderBody = JSON.serialize(clientComerzia);
            Boolean nuevo= (clientComerzia.DATOS.Id_de_cliente_Comerzzia==null ||clientComerzia.DATOS.Id_de_cliente_Comerzzia=='' ||clientComerzia.DATOS.Id_de_cliente_Comerzzia=='0');
            AccountsComerziaCallout.callout(cliente.Id,orderBody,nuevo);  
            // AccountsComerziaCallout.callout(cliente.Id,orderBody,true);  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
 
    }
}