public class CaseTriggerDispatcher implements TriggerDispatcher{
    private CaseHelper helper;
    public CaseTriggerDispatcher() {
        helper = new CaseHelper();
    }
    public void bulkBefore() { 
        System.debug('Case (Bulk Before)');
    }
    public void bulkAfter() {
        System.debug('Case (Bulk After)');
    }
    
    public void andFinally() {
        System.debug('Case (Finally)');
    }
    
    public void beforeInsert() {
        System.debug('Case (Before Insert)');
    }
    
    public void beforeUpdate() {
        System.debug('Case (Before Update)');
    }
    
    public void beforeDelete() {
        System.debug('Case (Before Delete)');
    }
    
    public void afterInsert() {
        System.debug('Case (After Insert)');
        helper.autoResponseEmail_afterInsert();
    }
    
    public void afterUpdate() {
        System.debug('Case (After Update)');
    }
    
    public void afterDelete() {
        System.debug('Case (After Delete)');
    }
    
    public void afterUndelete() {
		System.debug('Case (After Undelete)');        
    }
    
    public boolean isEnabled() {
        return Boolean.valueOf(Label.CaseTriggerEnabled);
    }
}