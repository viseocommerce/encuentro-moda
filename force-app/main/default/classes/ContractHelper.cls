public class ContractHelper {
	private List<Contract> oldList; 
    private List<Contract> newList; 
    private Map<id, Contract> oldMap;
    private Map<id, Contract> newMap;
    
    public ContractHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Contract>) Trigger.oldMap;
        this.newMap = (Map<id, Contract>) Trigger.newMap;
    }
}