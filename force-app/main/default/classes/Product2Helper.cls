public class Product2Helper {
	private List<Product2> oldList; 
    private List<Product2> newList; 
    private Map<id, Product2> oldMap;
    private Map<id, Product2> newMap;
    
    public Product2Helper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Product2>) Trigger.oldMap;
        this.newMap = (Map<id, Product2>) Trigger.newMap;
    }
}