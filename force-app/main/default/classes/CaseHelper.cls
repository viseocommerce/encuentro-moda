public class CaseHelper {
    private List<Case> oldList; 
    private List<Case> newList; 
    private Map<id, Case> oldMap;
    private Map<id, Case> newMap;
    
    public CaseHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Case>) Trigger.oldMap;
        this.newMap = (Map<id, Case>) Trigger.newMap;
    }
    
    public void autoResponseEmail_afterInsert(){
        system.debug('autoResponseEmail_afterInsert');
        
        List<Enable_Process__mdt> en_pro = [Select id,State__c from Enable_Process__mdt where Label = 'CaseEmail'];

        Boolean activeProcess = en_pro.size()>0? en_pro[0].State__c:false;
        
        if(activeProcess)
        {
            system.debug('autoResponseEmail_afterInsert : ACTIVE');
            List<Case> sendEmailCase = new  List<Case>();
            EmailTemplate et = [Select id from EmailTemplate where name = 'Respuesta automatica creacion por email' limit 1];
            
            for(Case actualCase : this.newList){
                if(actualCase.Origin == 'Email'){
                    sendEmailCase.add(actualCase);
                }
            }
            
            
            //Messaging.reserveSingleEmailCapacity(2);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            Messaging.SingleEmailMessage[] messagesList = new List<Messaging.SingleEmailMessage>();
            For(Case actualCase : sendEmailCase){
                System.debug('Id :' + actualCase.ID ); 
                system.debug(actualCase.ContactEmail);
                
                //Adress 
                String[] toAdresses = new String[]{actualCase.ContactEmail};
                    mail.setToAddresses(toAdresses); 
                
                //Obtain Direction
                //String directionDisplay = [Select id,label from CaseResponseEmail__mdt Limit 1].label;
                String directionDisplay = [Select id,label 
                                           from CaseResponseEmail__mdt where DeveloperName =:'daniel_ramos_viseo_com' Limit 1].label;
                //mail.setSenderDisplayName(directionDisplay);
                mail.setReplyTo(directionDisplay);
                //Obtain Direction ORG
                ID directionORGDisplay = [SELECT Id,Address,DisplayName
                                          FROM OrgWideEmailAddress 
                                          where DisplayName =: directionDisplay Limit 1].Id;
                mail.setOrgWideEmailAddressId(directionORGDisplay);
                
                //Template
                if(Test.isRunningTest()){
                    mail.setHtmlBody('Test Body');
                }
                else if(!Test.isRunningTest()){
                   mail.setTemplateId(et.ID); 
                }
              
                
                List<Id> contactIds = new List<Id>();
                contactIds.add(actualCase.ContactId);
                mail.setTargetObjectId(actualCase.ContactId); 
                
                
                List<Id> CaseIds = new List<Id>();
                CaseIds.add(actualCase.Id);
                mail.setWhatId(actualCase.Id);//Merge fields in template
                
                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setSaveAsActivity(false); 
                
                messagesList.add(mail);
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            system.debug('messagesList ' + messagesList);
            //List not null
            if(messagesList.size() > 0 && messagesList != null)
            {
                //Send
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messagesList);
                
                
                //Results
                if (results[0].success) 
                {
                    System.debug('The email was sent successfully.');
                } 
                else 
                {
                    System.debug('The email failed to send: ' +  results[0].errors[0].message);
                }   
            }    
        } 
    }
}