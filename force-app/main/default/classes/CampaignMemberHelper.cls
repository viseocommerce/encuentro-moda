public class CampaignMemberHelper {
	private List<CampaignMember> oldList; 
    private List<CampaignMember> newList; 
    private Map<id, CampaignMember> oldMap;
    private Map<id, CampaignMember> newMap;
    
    public CampaignMemberHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, CampaignMember>) Trigger.oldMap;
        this.newMap = (Map<id, CampaignMember>) Trigger.newMap;
    }
}