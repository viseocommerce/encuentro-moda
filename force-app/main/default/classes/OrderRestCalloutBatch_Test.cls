@isTest
public class OrderRestCalloutBatch_Test {
    @isTest
    static void OrderRestCalloutBatch_Test() {

        //Create Acc
        Account actualAcc = new account(
            FirstName = 'OOOO',
            LastName = 'AccTest',   
            Numero_de_tarjeta_CZZ__c = '1234',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId()
        );

        Insert actualAcc;

        //CREATE ORDER
        Order actualOrder = new Order(
            SFCC_Order_Number__c = 'EOLM2100013234',
            AccountId = actualAcc.ID,
            EffectiveDate = system.today(),
            SFCC_Order_Total__c = 23.88,
            Merchandize_total_tax__c = 3.47,
            Tax_total__c = 4.15,
            Order_SCCSync_Status__c = 'Created',
            Status = 'Draft',
            from_SFCC__c=true,
            Integrated_Comerzzia__c = false
        );

        insert actualOrder;


        //ORDERITEM1
        Order_Line_Item__c actOrdItem1 =  new Order_Line_Item__c(
            Order__c = actualOrder.ID,
            name = 'TestOrder',
            Base_price__c = null,
            Price_After_Item_Discount__c = 19.98,
            Price_After_Order_Discount__c = 19.98,
            Product_Id__c = '2023020150341M',
            Product_Name__c = 'CAMISETA ESPALDA DESCUBIERTA',
            Quantity__c = 2.0,
            Tax__c = 3.47,
            Tax_rate__c = 0.21,
            Unit_Price__c = 9.99,
            Variant_Info__c = 'Color:Verde Amazona ; Tamaño:M'
        );
        
        insert actOrdItem1;

        //ORDERITEM1
        Order_Line_Item__c actOrdItem2 =  new Order_Line_Item__c(
            Order__c = actualOrder.ID,
            Base_price__c = null,
            Price_After_Item_Discount__c = 19.98,
            Price_After_Order_Discount__c = 19.98,
            Product_Id__c = '2023020150341M',
            Product_Name__c = 'CAMISETA ESPALDA DESCUBIERTA',
            Quantity__c = 2.0,
            Tax__c = 3.47,
            Tax_rate__c = 0.21,
            Unit_Price__c = 9.99,
            Variant_Info__c = 'Color:Verde Amazona ; Tamaño:M'
        );
        
        insert actOrdItem2;


        //SHIPMENT1
        Shipment__c actShip1 = new Shipment__c(
            Order__c = actualOrder.ID,
            Shipment_No__c = '00008001',
            Shipping_Address__c = 'Prueba, asdad, 11 11100',
            Shipping_Method__c ='Envío a domicilio',
            Shipment_Total__c = 23.88,
            Shipping_Total_Tax__c = 0.68
        );
        insert actShip1;

        //SHIPMENT1
        Shipment__c actShip2 = new Shipment__c(
            Order__c = actualOrder.ID,
            Shipment_No__c = '00008001',
            Shipping_Address__c = 'Prueba, asdad, 11 11100',
            Shipping_Method__c ='Envío a domicilio',
            Shipment_Total__c = 23.88,
            Shipping_Total_Tax__c = 0.68
        );
        insert actShip2;


        //PAYMENT1
        Payment_Information__c actualPay1 = new Payment_Information__c(
            Order__c = actualOrder.ID,
            Amount_Charged_to_Card__c = 23.88,
            Payment_Method__c = 'CREDIT_CARD'
        );

        insert actualPay1;

        //PAYMENT1
        Payment_Information__c actualPay2 = new Payment_Information__c(
            Order__c = actualOrder.ID,
            Amount_Charged_to_Card__c = 23.88,
            Payment_Method__c = 'CREDIT_CARD'
        );

        insert actualPay2;


        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new OrderRestCalloutMockImpl());
            OrderRestCalloutBatch job = new OrderRestCalloutBatch();
            Database.executeBatch(job);
        Test.stopTest();

        //assertequals
        Order actualOrderObtain = [Select id,Integrated_Comerzzia__c,Integrated_Date_Comerzzia__c,name from order where SFCC_Order_Number__c = 'EOLM2100013234' Limit 1 ];
        system.assertequals(true,actualOrderObtain.Integrated_Comerzzia__c);
        system.assertequals(system.today(),actualOrderObtain.Integrated_Date_Comerzzia__c);

    }
    @isTest
    static void shedulerTest() {        
        Test.startTest();
        OrderRestCalloutBatchSheduler myClass = new OrderRestCalloutBatchSheduler();   
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, myClass);
        Test.stopTest();
   }
}