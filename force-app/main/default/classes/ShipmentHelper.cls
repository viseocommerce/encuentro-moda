public class ShipmentHelper{
	private List<Shipment__c> oldList; 
    private List<Shipment__c> newList; 
    private Map<id, Shipment__c> oldMap;
    private Map<id, Shipment__c> newMap;
    
    public ShipmentHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Shipment__c>) Trigger.oldMap;
        this.newMap = (Map<id, Shipment__c>) Trigger.newMap;
    }

    public void avoidDuplicateOrders() {
        List<Shipment__c> shipmentsToDelete = new List<Shipment__c>();
        List<id> ordersIds = new List<id>();
        List<String> shipmentIds = new List<String>();
        for(Shipment__c shipmentLine: this.newList) {
            ordersIds.add(shipmentLine.Order__c);
            shipmentIds.add(shipmentLine.Shipment_No__c);
        }
        List<Shipment__c> shipmentLines =[SELECT id,Order__c,Shipment_No__c FROM Shipment__c
                                                WHERE Order__c IN:ordersIds AND Shipment_No__c IN:shipmentIds
                                                ORDER BY Shipment_No__c];

        String orderId ;
        String shipmentId ;
        for (Shipment__c shipmentLine : shipmentLines) {
            if(shipmentLine.Order__c == orderId && shipmentLine.Shipment_No__c == shipmentId){
                shipmentsToDelete.add(shipmentLine);
            }
            orderId = shipmentLine.Order__c;
            shipmentId = shipmentLine.Shipment_No__c;
        }
        System.debug('shipments to delete :: '+ shipmentsToDelete);
        delete shipmentsToDelete;
    }
}