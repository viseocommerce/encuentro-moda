/*
*   @brief  This batch process integrates the order records with Comerzzia
*   @details N/A
*   @author Viseo
*   @see 

*   @version 1.0
*   @date 17/12/2020
*/
global class OrderRestCalloutBatch implements Database.Batchable<SObject> , Database.AllowsCallouts{
    //AllowsCallouts
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('TEST::Start OrderRestCalloutBatch');
        String query; //Query
        
        Boolean activeProcess = [Select id,State__c,label from Enable_Process__mdt  where Label = 'Order Integration'].State__c;
        
        if(test.isRunningTest()){
            activeProcess = true;
        }
        
        if(activeProcess){
            // List<String> idOrder =new List<String>{'EOLMSB22000019403'};
            query = 'SELECT Id,Integrated_Comerzzia__c,lastmodifieddate, Integrated_Date_Comerzzia__c,SFCC_Order_Number__c,AccountId,Account.Numero_de_tarjeta_CZZ__c,EffectiveDate, Date_Creation_Comerce__c'+
                ',Merchandize_total_tax__c, Tax_total__c,SFCC_Order_Total__c,Status,c_emo_site__c, Error_Comerzzia__c,CreatedDate,Account.Invitado__c ' +
                ',Reason__c,Notes__c,Decision__c,Payout__c,PickUpOnStoreCode__c,refundStatus__c,resultLog__c,shippingCost__c, Original_Number__c ' +
                'FROM ORDER  '+
                'WHERE Integrated_Comerzzia__c = false AND from_SFCC__c=true Order BY LastModifiedDate';
                // 'where Integrated_Comerzzia__c = false OR Modificar_en_Comerzzia__c=true Order BY LastModifiedDate DESC LIMIT 1';
                // 'where SFCC_Order_Number__c IN: idOrder';
        }
        system.debug(query);
        return DataBase.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Order> listOrder){
        system.debug('listOrder ' + listOrder);
        Map<ID,List<Order_Line_Item__c>> mapOrderOrderLineItem = new Map<ID,List<Order_Line_Item__c>>();
        Map<ID,List<Shipment__c>> mapShipment = new Map<ID,List<Shipment__c>>();
        Map<ID,List<Payment_Information__c>> mapPayment = new Map<ID,List<Payment_Information__c>>();
        
        //Obtain Related Records
        //Order_Line_Item__c
        For(Order_Line_Item__c actualOrderLine :  [Select id,Order__c,Base_price__c,Price_After_Item_Discount__c,Price_After_Order_Discount__c,
                                                   Product_Id__c,Product_Name__c,Quantity__c,Tax__c,Tax_rate__c,Unit_Price__c,Variant_Info__c,Adjusted_tax__c
                                                   FROM Order_Line_Item__c where Order__c in: listOrder]){
                                                       
                                                       List<Order_Line_Item__c> listOrderLineItem = new List<Order_Line_Item__c>();
                                                       //MAke map Account with users
                                                       if(mapOrderOrderLineItem.containsKey(actualOrderLine.Order__c)){
                                                           listOrderLineItem = mapOrderOrderLineItem.get(actualOrderLine.Order__c);
                                                           
                                                           listOrderLineItem.add(actualOrderLine);
                                                           
                                                           mapOrderOrderLineItem.put(actualOrderLine.Order__c,listOrderLineItem);
                                                       }
                                                       else if(!mapOrderOrderLineItem.containsKey(actualOrderLine.Order__c)){
                                                           listOrderLineItem.add(actualOrderLine);
                                                           mapOrderOrderLineItem.put(actualOrderLine.Order__c,listOrderLineItem);
                                                       }    
                                                   } 
        //Shipment__c
        For(Shipment__c actualShipment :  [Select id,Order__c,Shipment_No__c,Shipping_Address__c,Shipping_Method__c,
                                           Shipment_Total__c,Shipping_Total_Tax__c, Order__r.Producto_total__c, Order__r.SFCC_Order_Total__c,Order__r.Tax_total__c
                                           FROM Shipment__c where Order__c in: listOrder]){
                                               
                                               List<Shipment__c> listShipment = new List<Shipment__c>();
                                               //MAke map Account with users
                                               if(mapShipment.containsKey(actualShipment.Order__c)){
                                                   listShipment = mapShipment.get(actualShipment.Order__c);
                                                   
                                                   listShipment.add(actualShipment);
                                                   
                                                   mapShipment.put(actualShipment.Order__c,listShipment);
                                               }
                                               else if(!mapShipment.containsKey(actualShipment.Order__c)){
                                                   listShipment.add(actualShipment);
                                                   mapShipment.put(actualShipment.Order__c,listShipment);
                                               }    
                                           } 
        //Payment_Information__c
        For(Payment_Information__c actualPaymentInformation :  [Select id,Order__c,Amount_Charged_to_Card__c,Payment_Method__c
                                                                FROM Payment_Information__c where Order__c in: listOrder]){
                                                                    
                                                                    List<Payment_Information__c> listPaymentInformation = new List<Payment_Information__c>();
                                                                    //MAke map Account with users
                                                                    if(mapPayment.containsKey(actualPaymentInformation.Order__c)){
                                                                        listPaymentInformation = mapPayment.get(actualPaymentInformation.Order__c);
                                                                        
                                                                        listPaymentInformation.add(actualPaymentInformation);
                                                                        
                                                                        mapPayment.put(actualPaymentInformation.Order__c,listPaymentInformation);
                                                                    }
                                                                    else if(!mapPayment.containsKey(actualPaymentInformation.Order__c)){
                                                                        listPaymentInformation.add(actualPaymentInformation);
                                                                        mapPayment.put(actualPaymentInformation.Order__c,listPaymentInformation);
                                                                    }    
                                                                } 
        
        //CALL Webservice
        //
        System.debug('BEFORE API');
        for(Order actualOrder : listOrder){
            List<Order_Line_Item__c> listOrderItem = mapOrderOrderLineItem.get(actualOrder.ID);
            List<Shipment__c> listShipment = mapShipment.get(actualOrder.ID);
            List<Payment_Information__c> listPayment = mapPayment.get(actualOrder.ID);
            integer  result = 0;
            System.Debug('OrderStartDate '+actualOrder.CreatedDate);
            // if(!Test.isRunningTest())
            // {
                System.debug('ACABO DE LANZAR API');
                //Call Webservice
                if(actualOrder.SFCC_Order_Number__c !=null  &&listOrderItem!=null && listShipment!=null && listPayment!=null ){
                    result = OrderRestCallout.callout(actualOrder,listOrderItem,listShipment,listPayment);   
                }
            
            // }  
            // else{
            //     result = 1;
            // }  

            //Result OK
            if(result == 1){
                actualOrder.Integrated_Comerzzia__c = true;
                actualOrder.Integrated_Date_Comerzzia__c = system.today();
            }else{
                actualOrder.Integrated_Comerzzia__c = false;
            }
        }     
        
        update listOrder;
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
}