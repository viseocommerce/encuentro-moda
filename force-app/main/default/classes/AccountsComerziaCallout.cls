public  class AccountsComerziaCallout {
    public AccountsComerziaCallout() {

    }
    //  @future (callout=true)
     public static void callout(Id updateAccount,String orderBody,Boolean nuevo) {
        List<Account> accountsToUpdate =new List<Account>(); 
        // String endpoint = nuevo?'http://www-pre.encuentromoda.net/sap/ws/sfc2cmz_clientenuevo':'http://www-pre.encuentromoda.net/sap/ws/sfc2cmz_clienteactualiza';// = 'callout:CREDENTIALNAME/SERVICE';//EndPoint Request
        String endpoint = nuevo?Label.endPointClienteNuevoComerzzia:Label.endPointClienteActualizaComerzzia;// = 'callout:CREDENTIALNAME/SERVICE';//EndPoint Request
        String operation = nuevo? 'POST':'PUT';
        System.debug('EndPoint :: '+ endpoint);
        String responseBody;//Response result
        // String orderBody;//Request Body
        // Blob headerValue = Blob.valueOf('usrAsteria' + ':' + 'poduser01');
        Blob headerValue = Blob.valueOf(Label.usuarioComerzzia + ':' + Label.passwordComerzzia);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        System.debug('AUTORIZATION: '+ authorizationHeader);
        HttpRequest request = new HttpRequest();//Request petition
        HttpResponse response = new HttpResponse();//Response Request
        Http http = new Http();

        Account accountUpdate = new Account();
        // accountUpdate.Id = clientComerzia.DATOS.IdSalesforce;
        accountUpdate.Id = updateAccount;
        //GET STRING / JSON
        // orderBody = JSON.serialize(clientComerzia);
        System.debug('JSON sent out with Account vals: '+ orderBody);
        
        ///REQUEST
        try{
            //Request
            request.setHeader('Authorization', authorizationHeader);
            request.setMEthod(operation);
            // request.setMEthod('PUT');
            // request.setMEthod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setEndpoint(endpoint);
            request.setBody(Accents.removeDiacritics(orderBody));
            // request.setBody(orderBody);

            system.debug('Request: '+ request);
            system.debug('Request Body: '+ request.getBody());
            accountUpdate.Request_Comerzzia__c = request.getBody();

            response = http.send(request);
            responseBody = response.getBody();

            //DEBUG RESPONSE
            System.debug('Response StatusCode: '+ response.getstatusCode());
            System.debug('Response Body: '+ response.getBody());
            accountUpdate.Response_Comercia__c = response.getBody();
            if (response.getStatusCode() == 200) {

                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

                Object fieldList = (Object)JSON.deserializeUntyped(response.getBody()); 
                Map<String,Object> data = (Map<String,Object>)fieldList;
                Map<String, Object> productMap = (Map<String, Object>)data.get('RespuestaCMZ');
                Boolean successUpdate = (Boolean) productMap.get('success');
                Integer id_Comerzzia =(Integer) productMap.get('Id_de_cliente_Comerzzia');
                if(successUpdate){
                    accountUpdate.Error_Comerzzia__c= false;
                    accountUpdate.Ultima_modificaci_n_comerzzia__c= Datetime.now();
                }else {
                    accountUpdate.Error_Comerzzia__c= true;                    
                }
                if(nuevo){                    
                    accountUpdate.Id_de_cliente_Comerzzia_CZZ__c=String.valueOf(id_Comerzzia)!='0'?String.valueOf(id_Comerzzia):null;
                }
            }else{
                accountUpdate.Error_Comerzzia__c= true;
            }
            accountsToUpdate.add(accountUpdate);
        
            update accountsToUpdate;                  
        }catch (System.Exception e){
            system.debug('catch');
            System.debug('ERROR GeneralAPI:' + e.getTypeName() + ' - ' + e.getMessage() + ' - ' + e.getStackTraceString() + ' - ' + responseBody);
            accountUpdate.Error_Comerzzia__c= true;
            accountUpdate.Response_Comercia__c = response.getBody();
            accountUpdate.Id_de_cliente_Comerzzia_CZZ__c =nuevo?null:accountUpdate.Id_de_cliente_Comerzzia_CZZ__c;
            accountsToUpdate.clear();
            accountsToUpdate.add(accountUpdate);
            update accountsToUpdate;                  
        }
    }
}