global class AccountsComerziaBatchSheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        AccountsComerziaBatch b = new AccountsComerziaBatch(); 
        database.executebatch(b,1); 
    }
}