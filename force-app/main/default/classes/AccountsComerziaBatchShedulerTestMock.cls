@isTest
global class AccountsComerziaBatchShedulerTestMock implements HttpCalloutMock{
    global HttpResponse respond(HttpRequest req){
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"RespuestaCMZ": {"Id_de_cliente_Comerzzia": 10299,"success": true,"error": "Se ha procesado correctamente el fidelizado de Salesforce"}}');
        res.setStatusCode(200);
        return res;
    } 
}