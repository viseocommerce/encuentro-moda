public class AddressHelper {
    private List<Address__c> oldList; 
    private List<Address__c> newList; 
    private Map<id, Address__c> oldMap;
    private Map<id, Address__c> newMap;
        
    public AddressHelper() {
        this.oldList = Trigger.old;
        this.newList = Trigger.new;
        this.oldMap = (Map<id, Address__c>) Trigger.oldMap;
        this.newMap = (Map<id, Address__c>) Trigger.newMap;
    }
    
    public void setAddressForCustumer() {
        List<Account> updateAccounts = new List<Account>();
        System.debug('Enter Insert Adrress');
        for(Address__c add: this.newList) {
            System.debug('Update account');
            if(add.Sync_Address__c== true){
                Account updateAccount =new Account(id=add.Account__c,
                Nombre_de_via_CZZ__c = add.Address_Line_1__c,
                Complemento_CZZ__c = add.Address_Line_2__c,
                Poblacion_CZZ__c = add.City__c,
                Codigo_postal_CZZ__c= add.Postal_Code__c,
                Provincia_CZZ__c= add.state_Name__c,
                Pais_CZZ__c = add.Country__c);
                add.Sync_Address__c= true;

                updateAccounts.add(updateAccount);
            }
            add.SFCC_AddressUpdate__c = false;
        }
        if(updateAccounts.size()>0){
            update updateAccounts;
        }
    }
    public void updateAddressForCustumer() {     
        List<Account> updateAccounts = new List<Account>();
        for(Address__c add: this.newList) {
            // if((add.update_by_account__c==this.oldMap.get(add.Id).update_by_account__c) && add.Sync_Address__c == true){
            if((add.update_by_account__c== false) && add.Sync_Address__c == true){
                Account updateAccount =new Account(id=add.Account__c,
                Nombre_de_via_CZZ__c = add.Address_Line_1__c,
                Complemento_CZZ__c = add.Address_Line_2__c,
                Poblacion_CZZ__c = add.City__c,
                Codigo_postal_CZZ__c= add.Postal_Code__c,
                Provincia_CZZ__c= add.state_Name__c,
                Pais_CZZ__c = add.Country__c,
                update_by_address__c = true);
                add.Sync_Address__c= true;

                updateAccounts.add(updateAccount);           
            }else{
                add.update_by_account__c = false;                
            }
            if(updateAccounts.size()>0){
                update updateAccounts;
            }
        }
    }
}