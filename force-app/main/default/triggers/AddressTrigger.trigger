trigger AddressTrigger on Address__c (before insert, after insert,
                                        before update, after update,
                                        before delete, after delete,
                                        after undelete) {
TriggerFactory.createTriggerDispatcher(Address__c.sObjectType);
}