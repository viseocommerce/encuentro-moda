/**********************************************************************************
* @author       
* @date         05/08/2019
* @group        Integration Log
* @description  Trigger of Integration_Log__e
* @Revision
**********************************************************************************/
trigger IntegrationLogEventTrigger on Integration_Log_Event__e (after insert) {
    //IntegrationLogUtility.saveIntegrationLogs(Trigger.new);
}