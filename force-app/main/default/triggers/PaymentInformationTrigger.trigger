trigger PaymentInformationTrigger on Payment_Information__c (before insert, after insert,
                                                                before update, after update,
                                                                before delete, after delete,
                                                                after undelete) {
TriggerFactory.createTriggerDispatcher(Payment_Information__c.sObjectType);
}