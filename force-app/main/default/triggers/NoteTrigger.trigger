trigger NoteTrigger on Note (	before insert, after insert,
                                before update, after update,
                                before delete, after delete,
                                after undelete) {
	TriggerFactory.createTriggerDispatcher(Note.sObjectType);
}