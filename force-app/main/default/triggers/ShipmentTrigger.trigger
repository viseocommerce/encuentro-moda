trigger ShipmentTrigger on Shipment__c (before insert, after insert,
                                        before update, after update,
                                        before delete, after delete,
                                        after undelete) {
TriggerFactory.createTriggerDispatcher(Shipment__c.sObjectType);
}