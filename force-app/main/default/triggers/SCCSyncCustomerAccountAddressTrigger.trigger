trigger SCCSyncCustomerAccountAddressTrigger on Address__c (before insert, before update) {
    SCCFileLogger logger = SCCFileLogger.getInstance();
    Boolean result;
    try{	
        if(trigger.IsInsert){
            
        }
        if(trigger.IsUpdate){
            List<Address__c> newAddress = trigger.new;
            List<Address__c> oldAddress = trigger.old;
            List<id> addressIds = new List<id>();
            Map<String, Object> patchDataMap = new Map<String, Object>();
            Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Address__c.fields.getMap(); 
            for(Integer i = 0; i < newAddress.size(); i++) {
                addressIds.add(newAddress.get(i).id);
                // this is avoid calling future method when object updated by webservice from CC.                
            }
            Map<id, Address__c> getAddressAccount = new Map<id, Address__c>([SELECT id,Name,Account__r.SFCC_Customer_Number__pc,Account__r.SFCC_Customer_Id__pc FROM Address__c WHERE id IN:addressIds]);

            for(Integer i = 0; i < newAddress.size(); i++) {
                Address__c newAdd = newAddress.get(i);
                Address__c oldAdd = oldAddress.get(i);
                // this is avoid calling future method when object updated by webservice from CC.
                if(!newAdd.SFCC_AddressUpdate__c){
                    for (String str : fieldMap.keyset()) {
                        logger.debug('SCCSyncCustomerAccountAddressTrigger.IsUpdate', 'Field name: '+str +'. New value: ' + newAdd.get(str) +'. Old value: '+oldAdd.get(str));
                        if(newAdd.get(str) != oldAdd.get(str)){
                            logger.debug('SCCSyncCustomerAccountAddressTrigger.IsUpdate', 'Patching commerce cloud for field '+ str);
                            patchDataMap.put(str, newAdd.get(str)); 
                        }
                    }
                    if(!patchDataMap.isEmpty()){
                        System.debug('MAP INFO :: '+ patchDataMap);
                        System.debug('NUMBER ORDER ADDRESS TRIGGER:: '+ getAddressAccount.get(newAdd.id).Account__r.SFCC_Customer_Number__pc);
                        //Call Commerce Cloud patch
                        result = SCCAccountImpl.patchCustProfileAddress(patchDataMap, getAddressAccount.get(newAdd.id));                      
                    } 
                }
                newAdd.SFCC_AddressUpdate__c = false;
            }        
        }
        }catch(Exception e){
        logger.error('SCCSyncCustomerAccountTrigger', 'Exception message : '
                        + e.getMessage() + ' StackTrack '+ e.getStackTraceString());   		
    }finally{
        logger.flush();
    } 
}