trigger OrderLineItemTrigger on Order_Line_Item__c (before insert, after insert,
                                            before update, after update,
                                            before delete, after delete,
                                            after undelete) {
TriggerFactory.createTriggerDispatcher(Order_Line_Item__c.sObjectType);
}