trigger OrderItemTrigger on OrderItem (	before insert, after insert,
                                   		before update, after update,
                                   		before delete, after delete,
                                   		after undelete) {
	TriggerFactory.createTriggerDispatcher(OrderItem.sObjectType);
}