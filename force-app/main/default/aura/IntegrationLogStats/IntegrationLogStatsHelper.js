({
	addEvent : function(component, payload) {        
        var config = window.chartLogs.config;
        var moduleName = payload.Module__c;
        var datasetExists = false;
        config.data.labels.forEach(function(label) {
            if(label == moduleName) {
            	datasetExists = true;
                return;
            }
        });
        if(!datasetExists) {
            config.data.labels.push(moduleName);
            config.data.datasets[0].data.push(0);                                
            config.data.datasets[1].data.push(0);
        }
        
        var index = -1;
        config.data.labels.forEach(function(label, i) {
            if(label == moduleName) {
            	index = i;
            }
        });
        if(index < 0) return;
        
        var total = config.data.datasets[0].data[index];
        config.data.datasets[0].data[index] = total + 1;
        
        if(payload.Error_Message__c) {
            var errors = config.data.datasets[1].data[index];
        	config.data.datasets[1].data[index] = errors + 1;
        }
        
        window.chartLogs.update();
    }
})