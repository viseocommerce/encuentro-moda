({
	addDataset : function(component, moduleName) {        
        var config = window.chart.config;
        var datasetExists = false;
        config.data.datasets.forEach(function(dataset) {
            if(dataset.label == moduleName) {
            	datasetExists = true;    
                return;
            }
        })
        if(datasetExists) return;
        
        var color = Chart.helpers.color;
        var chartColors = {
            red: 'rgb(255, 99, 132)',
            blue: 'rgb(54, 162, 235)',
            orange: 'rgb(255, 159, 64)',
            purple: 'rgb(153, 102, 255)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            grey: 'rgb(201, 203, 207)'
        };
        var colorNames = Object.keys(chartColors);
        
		var colorName = colorNames[config.data.datasets.length % colorNames.length];
        var newColor = chartColors[colorName];
        var newDataset = {
            label: moduleName,
            backgroundColor: color(newColor).alpha(0.5).rgbString(),
            borderColor: newColor,
            borderWidth: 1,
            fill: false,
			cubicInterpolationMode: 'monotone',
            data: []
        };
        
        config.data.datasets.push(newDataset);
		window.chart.update();        
	}
})