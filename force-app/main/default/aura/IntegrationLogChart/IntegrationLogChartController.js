({
	// Sets an empApi error handler on component initialization
    onInit : function(component, event, helper) {
        // Get the empApi component
        const empApi = component.find('empApi');

        // Uncomment below line to enable debug logging (optional)
        // empApi.setDebugFlag(true);

        // Register error listener and pass in the error handler function
        empApi.onError($A.getCallback(error => {
            // Error can be any type of error (subscribe, unsubscribe...)
            console.error('EMP API error: ', error);
        }));
            
        // Get the channel from the input box
        const channel = component.get('v.channel');
        // Replay option to get new events
        const replayId = -1;

        // Subscribe to an event
        empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
            // Process event (this is called each time we receive an event)
            //console.log('Received event ', JSON.stringify(eventReceived));
            
            if(!eventReceived.data.payload.Error_Message__c) return;
            
            var moduleName = eventReceived.data.payload.Module__c;
            helper.addDataset(component, moduleName);   
            
            var events = component.get("v.events");
            if(!events[moduleName]) events[moduleName] = [];
            events[moduleName].push(eventReceived.data.payload);
            console.log('Total events: ' + events[moduleName].length);
            component.set("v.events", events);            
        }))
        .then(subscription => {
            // Confirm that we have subscribed to the event channel.
            // We haven't received an event yet.
            console.log('Subscribed to channel ', subscription.channel);
            // Save subscription to unsubscribe later
            component.set('v.subscription', subscription);
        });
    },
            
    onScriptsLoaded : function(component, event, helper) {
        Chart.plugins.unregister(ChartStreaming);
        
        var onRefresh = $A.getCallback(function(chart) {
            var events = component.get("v.events");
            if(!events) return;
            
            chart.config.data.datasets.forEach(function(dataset) {
                var moduleEvents = events[dataset.label];
                if(!moduleEvents) {
                    /*dataset.data.push({
                        x: Date.now(),
                        y: 0
                    });*/
                    return;
                }
                
                var data = new Map();
                moduleEvents.forEach(function(payload) {
                    if(!data.get(payload.CreatedDate)) data.set(payload.CreatedDate,0);
                    data.set(payload.CreatedDate,data.get(payload.CreatedDate)+1);
                });
                
                /*data.forEach(function(value,key) {
                	dataset.data.push({
                        x: key,
                        y: value,
                        r: value * 10,
                        error: 'Error'
                    });
                });*/

				moduleEvents.forEach(function(payload) {
                    var y = data.get(payload.CreatedDate);
                    if(!y) y = 1;
                    dataset.data.push({
                        x: payload.CreatedDate,
                        y: y,
                        r: y * 10,
                        error: payload.Error_Message__c
                    });
                });                
            	
            });
            component.set("v.events",{});
        })
        
        var color = Chart.helpers.color;
        var config = {
            plugins: [ChartStreaming],
            type: 'bubble',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                title: {
                    display: true,
                    text: 'Integration Log Errors',
                    position: 'bottom'
                },                
                legend: {
                    display: true,
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        type: 'realtime',
                        realtime: {
                            duration: 60000,
                            refresh: 1000,
                            delay: 1000,
                            onRefresh: onRefresh
                        }
                    }],
                    yAxes: [{
                        type: 'linear',
                        displaye: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'value'
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 10
                        }
                    }]
                },
                tooltips: {
                    mode: 'nearest',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].label || '';
        
                            if (label) {
                                label += ': ';
                            }
                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].error;
                            return label;
                        }
                    }
                },
                hover: {
                    mode: 'nearest',
                    intersect: false
                },
                plugins: {
                    datalabels: {
                        backgroundColor: function(context) {
                            return context.dataset.backgroundColor;
                        },
                        borderRadius: 4,
                        clip: true,
                        color: 'white',
                        font: {
                            weight: 'bold'
                        },
                        formatter: function(value) {
                            return value.error;
                        }
                    }
                }
            }
        };
        
        var ctx = document.getElementById('chart').getContext('2d');
		window.chart = new Chart(ctx, config);
    }    
})