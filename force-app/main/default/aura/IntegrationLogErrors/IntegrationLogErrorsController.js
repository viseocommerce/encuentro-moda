({
	onInit : function(component, event, helper) {
		// Get the empApi component
        const empApi = component.find('empApi');

        // Uncomment below line to enable debug logging (optional)
        // empApi.setDebugFlag(true);

        // Register error listener and pass in the error handler function
        empApi.onError($A.getCallback(error => {
            // Error can be any type of error (subscribe, unsubscribe...)
            console.error('EMP API error: ', error);
        }));
            
        // Get the channel from the input box
        const channel = component.get('v.channel');
        // Replay option to get new events
        const replayId = -1;

        // Subscribe to an event
        empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
            // Process event (this is called each time we receive an event)
            console.log('Received event ', JSON.stringify(eventReceived));
            
            helper.addError(component, eventReceived.data.payload, eventReceived.data.event);
        }))
        .then(subscription => {
            // Confirm that we have subscribed to the event channel.
            // We haven't received an event yet.
            console.log('Subscribed to channel ', subscription.channel);
            // Save subscription to unsubscribe later
            component.set('v.subscription', subscription);
        });
	},
            
    onToggleExpand : function(component, event, helper) {
        var id = event.srcElement.getAttribute("data-id");
        var errors = component.get("v.errors");
        for(var i = 0; i < errors.length; i++) {
            if(errors[i].Id == id) {
                errors[i].expanded = !errors[i].expanded;
                break;
            }
        }
        component.set("v.errors", errors);
    },
            
    onDelete : function(component, event, helper) {
        component.set("v.errors", []);
    }
            
})