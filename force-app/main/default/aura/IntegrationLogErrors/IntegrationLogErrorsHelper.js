({
	addError : function(component, payload, event) {
        if(!payload.Error_Message__c) return;
        
        payload.expanded = false;
        payload.Id = event.replayId;
		
        var errors = component.get("v.errors");
        errors.unshift(payload);
        component.set("v.errors", errors);
	}
})